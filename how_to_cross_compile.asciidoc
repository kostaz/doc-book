Cross-Build in OptiSwitch Environment
=====================================

== Build OptiSwitch
```
git clone ssh://git@bitbucket.mrv.co.il:7999/osv2018/distribution.git
git clone ssh://git@bitbucket.mrv.co.il:7999/osv2018/os-v.git
git clone ...
```

Setup build environment and perform a full build of OptiSwitch using `mkall.sh ...`.

== Create Hello World
```
cd .../build_env/dist/build-gcc-4.8.5-glibc-2.19-cpu-_msys
mkdir hello_world
cd hello_world
touch main.c
vim main.c
```

== Cross-Build Hello World
```
.../toolchain/bin/arm-marvell-linux-gnueabi-gcc main.c -o main.o
```

== Verify Cross-Build
```
file main.o
main.o: ELF 32-bit LSB executable, ARM, EABI5 version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.3, for GNU/Linux 2.6.16, BuildID[sha1]=e3ebec6362c63d1ff6822c39ce9c529debd57d97, not stripped
```
