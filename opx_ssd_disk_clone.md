Cloning OPX SSD disk (to another fresh SSD disk)
================================================

#### The instructions below cover disk cloning of USB **and** SSD disks.

#### Preminiary
The procedure is performed on the linux machine (or virtual linux machine).
Clean the Linux system log using the below bash command:
```
sudo dmesg -c > /dev/null
```

#### Note the exact name of the origin disk
Insert the first disk that is being used as the source of copy.
Wait for a couple of seconds and perform the below bash command:
```
sudo dmesg -c > /dev/null # to clean linux system log
dmesg
```
Note a freshly inserted disk letter, for example, `b` in `/dev/sdb`.

#### Note the exact name of the destination disk
```
sudo dmesg -c > /dev/null # to clean linux system log
dmesg
```
Note a freshly inserted disk letter, for example, `c` in `/dev/sdc`.

---

#### Check disk sizes
Use the below command to verify that the size of both disks is the same:
```
sudo fdisk -l /dev/sdb

Disk /dev/sdb: 29.8 GiB, 32017047552 bytes, 62533296 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xb289f6bb

Device     Boot    Start      End  Sectors  Size Id Type
/dev/sdb1  *        2048   206847   204800  100M 83 Linux
/dev/sdb2         206848   411647   204800  100M 83 Linux
/dev/sdb3         411648   616447   204800  100M 83 Linux
/dev/sdb4         616448 62533294 61916847 29.5G  5 Extended
/dev/sdb5         618496  8810495  8192000  3.9G 83 Linux
/dev/sdb6        8812544  9836543  1024000  500M 83 Linux
/dev/sdb7        9838592 13934591  4096000    2G 83 Linux
/dev/sdb8       13936640 22128639  8192000  3.9G 83 Linux
/dev/sdb9       22130688 23154687  1024000  500M 83 Linux
/dev/sdb10      23156736 27252735  4096000    2G 83 Linux
/dev/sdb11      27254784 62533294 35278511 16.8G 83 Linux
```

Note in the line `Disk /dev/sdb: 29.8 GiB, 32017047552 bytes, 62533296 sectors`
the amount of bytes and sectores. Verify that it is identical between
origin and destination disks.

---

Copying from `/dev/sdb` (good disk) to `/dev/sdc` (bad or fresh disk).

#### Install `ddrescue` linux util:
```
sudo apt-get install gddrescue
```
Note: The exact name is `gddrescue`!

#### The command in bash is:
```
sudo ddrescue -D --force -v -r1 /dev/sdb /dev/sdc
```

#### Example output is:
```
sudo ddrescue -D --force -v -r1 /dev/sdb /dev/sdc
GNU ddrescue 1.19
About to copy 32017 MBytes from /dev/sdb to /dev/sdc.
    Starting positions: infile = 0 B,  outfile = 0 B
    Copy block size: 128 sectors       Initial skip size: 128 sectors
Sector size: 512 Bytes

Press Ctrl-C to interrupt
rescued:   604307 kB,  errsize:       0 B,  current rate:   30343 kB/s
   ipos:   604307 kB,   errors:       0,    average rate:   30215 kB/s
   opos:   604307 kB, run time:      20 s,  successful read:       0 s ago
Copying non-tried blocks... Pass 1 (forwards)
```


Two-step clone
==============

Disk cloning may be done in two steps - (1) copy USB/SSD disk to PC
as a binary file and then (2) burn this binary image to another
USB/SSD disk.

To clone USB/SSD disk to PC as a binary file - insert disk in PC and then:
```
sudo ddrescue -D --force /dev/sdc ~/disk_dd_image
```
Note: `c` letter in `/dev/sdc` should be verified using the instructions
above.

Now, push out the disk and insert a new USB/SSD disk and then:
```
sudo ddrescue -D --force ~/disk_dd_image /dev/sdc
```
Note: `c` letter in `/dev/sdc` should be verified using the instructions
above.


#### Reference
http://www.kossboss.com/linux---how-to-clone-a-disk-with-ddrescue---dnu-ddrescue-also-known-as-gddrescue---the-better-ddrescue-tool
