How to compile and sanity-test keepalived
=========================================

### Compile
```
git clone ssh://git@bitbucket.mrv.co.il:7999/opx/keepalived.git
cd keepalived

export clovis_sdk=clovis_2015_lk4_4
export PATH=$PATH:/home/$USER/$clovis_sdk/buildtools/i686-nptl-linux-gnu/bin
export TARGET_SYSROOT=/home/$USER/$clovis_sdk/buildtools/i686-nptl-linux-gnu
export CFLAGS="-I$TARGET_SYSROOT/include -L$TARGET_SYSROOT/lib"

./configure \
--host=i686-nptl-linux-gnu \
--prefix=/tmp/keepalived \
--disable-silent-rules \
--disable-dependency-tracking \
--disable-libiptc

make
make install
```

The built binary file is found at:
```
ls -ls ./keepalived/keepalived

file ./keepalived/keepalived
./keepalived/keepalived: ELF 32-bit LSB executable, Intel 80386,
  version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.2,
  for GNU/Linux 4.4.10, not stripped
```

Copy this file to the target `/sbin` directory.


### Run keepalived

Create on target (Agema or OPX) the below configuration:
```
A1# write terminal
Building configuration...

Current configuration:
!
line vty
 no exec-timeout
hostname A1
!
port eth-1/0/49
 enable
!
interface out-of-band eth0
 ip address 10.100.1.242/24
 enable
!
interface inet eth-1/0/49.1
 ip address 10.10.10.10/24
 outer-tags 10
 enable
!
ip route default 10.100.1.254
!
ip filtering
 allow service ssh
 allow service telnet
!
clock timezone israel
!
```

Enter the linux and create configuration file for keepalived:
```
root@A1:~# cat /etc/keepalived/keepalived.conf
vrrp_instance i1_0_49.1 {
	state BACKUP
	interface eth0
	use_vmac
	virtual_router_id 1
	virtual_ipaddress {
		10.10.10.1/24 brd 10.10.10.0
	}
	priority 100
	advert_int 1
}
```

Now, run the keepalived daemon in the below way:
```
keepalived -P -D -S 1
```

Verify that a new ip interface (`inet 10.10.10.1`) is added:
```
$ ip address show
   ...
3: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:18:23:30:e0:b6 brd ff:ff:ff:ff:ff:ff
    inet 10.100.1.242/24 brd 10.100.1.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet 10.10.10.1/24 brd 10.10.10.0 scope global eth0
       valid_lft forever preferred_lft forever
   ...
```

And, last, verify with `ifconfig` that a new interface is
created named `vrrp.1`.
