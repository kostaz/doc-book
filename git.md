Note:
=====
 - This document is written using Git Flavored Markdown (GFM).
 - https://guides.github.com/features/mastering-markdown/
 - https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet


Find all git repositories:
==========================

`find . -type d -name "\.git" | sort`

Example:

```
[kostaz@plex] [~/My/MRV/NG/Dev/git/MRV/2016_07_28]

ls -ls
total 12
4 drwxrwxr-x  6 kostaz kostaz 4096 Jul 28 11:54 build_env_bc2
4 drwxrwxr-x 13 kostaz kostaz 4096 Jul 28 11:54 distribution
4 drwxrwxr-x 15 kostaz kostaz 4096 Jul 28 12:10 os-v
```

```
[kostaz@plex] [~/My/MRV/NG/Dev/git/MRV/2016_07_28]

find . -type d -name "\.git" | sort
./build_env_bc2/dist/build-gcc-4.6.4-glibc-2.15-cpu-_msys/.git
./build_env_bc2/dist/build-gcc-4.6.4-glibc-2.15-cpu-_msys/linux-3.4.69/.git
./build_env_bc2/dist/build-gcc-4.6.4-glibc-2.15-cpu-_msys/u-boot-2013.01-msys-v13.git/.git
./build_env_bc2/install/.git
./build_env_bc2/scripts/.git
./distribution/.git
./os-v/.git

[kostaz@plex] [~/My/MRV/NG/Dev/git/MRV/2016_07_28]
```
--------------------------------------------------------------------------------

Topics
======


Git basics
==========

 - What happens on git clone?
 - What happens on git fetch?
 - What happens on git push?
 - What happens on git pull?
 - What happens on git pull --rebase?


General tips and shortcuts
==========================

 - Use CTRL-R to search bash command history

 - Use super short find to find files: `f`
   ```
   $ f stapi
   ```
    - Implemented in `~/.bashrc` file as function
    - https://github.com/kostaz/env/blob/master/.bashrc#L193
    - Thanks to Alex Rozin! :-)
    - Find function:
      ```
      function f()
      {
          term=$@
          find . -iname "*${term}*"
      }
      ```

 - Strict super short find: `sf` (case sensitive)
   ```
   $ sf sTapi
   ```
    - Strict find function:
      ```
      function sf()
      {
          term=$@
          find . -name "${term}"
      }
      ```

 - Use git aliases in ~/.bashrc
    - https://github.com/kostaz/env/blob/master/.bashrc#L171
    - alias gg='git grep -in --untracked '
    - alias sgg='git grep -n --untracked '
    - alias gs='git status'
    - alias gdc='git diff --cached --ignore-all-space '
    - alias gdcI='git diff --cached '
    - alias gd='git diff --ignore-all-space '
    - alias gdI='git diff '
    - alias gdt='git difftool '
    - alias ga='git add '
    - alias gb='git branch'

 - Most useful git aliases:
    - gs (git status)
    - gd (git diff)
    - gb (git branch)
    - ga (git add)


Basic git commands
==================

 - Use `git status` almost after every git command (`gs`)

 - Use git log with `--max-count`
   ```
   git log --oneline --decorate --max-count=30
   ```

 - git blame
   ```
   $ git blame -- ./pss/pss/SW/prestera/mainDrv/src/prestera/sTapi/sTapi_if.c
   ```

 - Track file history with `git log -p`
   ```
   git log -p ./pss/pss/SW/prestera/mainDrv/src/prestera/sTapi/sTapi_if.c
   git log --full-diff -p ./pss/pss/SW/prestera/mainDrv/src/prestera/sTapi/sTapi_if.c
   ```

 - Reviewing last commit
   ```
   git show --name-status
   git diff HEAD~1..HEAD
   git diff HEAD~1..HEAD -- filename
   git difftool HEAD~1..HEAD
   git difftool HEAD~1..HEAD -- filename
   git difftool HEAD~1..HEAD -- pss/pss/SW/prestera/mainAppDemo/src/appDemo/boardConfig/gtMRVDxBobk.c
   git difftool --tool=meld HEAD~1..HEAD -- pss/pss/SW/prestera/mainAppDemo/src/appDemo/boardConfig/gtMRVDxBobk.c
   ```

 - Updating (amending) last commit
   ```
   git commit --amend
   ```


Git branches
============

 - How many branches are there?
   ```
   git branch --all
   git branch -a
   gb -a
   git branch -r
   ```


Deleting stuff
==============

 - Delete all git tags
   ```
   git tag | xargs git tag --delete 
   ```

 - Delete local copies of unneeded remote branches
   ```
   git branch -r -d origin/feature/modular_aleksey
   gb -r -d origin/feature/modular_aleksey
   ```
   - Note: `git fetch` creates remote-tracking branches anew.

 - Deleting remote branch (on git server)
   ```
   git push origin --delete branch_name
   ```

 - Delete all unneeded files from local
   ```
   git ls-files --exclude-standard --others
   git ls-files --exclude-standard --others | xargs rm -rf
   ```

 - TBD: Clean the files from .gitignore.
   ```
   git clean -X -f
   ```

 - TBD: Before deleting untracked files/directory,
        do a dry run to get the list of these files/directories
   ```
   git clean -n
   ```

 - TBD: Forcefully remove untracked files
   ```
   git clean -f
   ```

 - TBD: Forcefully remove untracked directory
   ```
   git clean -f -d
   ```

 - TBD: Alternatives:
   ```
   git clean -df
   ```

 - TBD: Dry run. (any command that supports dry-run flag should do.)
   ```
   git clean -fd --dry-run
   ```


Listing files
=============

 - Print files using `git ls-files`
    - Possible parameters for `git ls-files`:
      ```
      --exclude-standard
          Add the standard Git exclusions: .git/info/exclude,
                                           .gitignore in each directory,
                                           and the user’s global exclusion file.

      -o, --others
          Show other (i.e. untracked) files in the output

      -c, --cached
          Show cached files in the output (default)

      -d, --deleted
          Show deleted files in the output

      -m, --modified
          Show modified files in the output

      -o, --others
          Show other (i.e. untracked) files in the output

      -i, --ignored
          Show only ignored files in the output. When showing files in the index, print only those matched by an exclude pattern. When showing "other"
          files, show only those matched by an exclude pattern.

      -s, --stage
          Show staged contents' object name, mode bits and stage number in the output.
      ```

 - TBD: Show all tracked files
   ```
   git ls-files -t
   ```

 - TBD: Show all untracked files
   ```
   git ls-files --others
   ```

 - TBD: Show all ignored files
   ```
   git ls-files --others -i --exclude-standard
   ```

 - Viewing all commits in all branches
   ```
   git log --oneline --decorate --graph --branches --all --first-parent
   ```

 - Pick and apply any commit from any branch
   ```
   git cherry-pick
   ```

 - Flow of `git am` after `git format-patch`
   ```
   git am my.patch
   git apply --reject my.patch
   git add filename1 filename2 ...
   git am --continue
   ```

 - Compare file between different branches
   ```
   git diff filename branch1:branch2
   ```

 - Cancelling some commit
   ```
   git revert
   ```

 - git am for a lot of patches (git format-patch -100)
   ```
   git format-patch -6 --start-number 27 --output-directory=../for_kernel_git/patches/
   ```

 - TBD: Rename branch

 - TBD: git am -p1 --directory=...

 - TBD: git tag, git push --tags

 - TBD: Delete local tag
   ```
   git tag -d <tag-name>
   ```

 - TBD: What changed since two weeks?
   ```
   git log --no-merges --raw --since='2 weeks ago'
   git whatchanged --since='2 weeks ago'
   ```

 - TBD: Delete remote tag
   ```
   git push origin :refs/tags/<tag-name>
   ```

 - TBD: git grep in all changes from last month

 - TBD: git commit message template

 - TBD: git log by time and author

 - TBD: git reflog

 - TBD: git bisect

 - TBD: git rebase

 - TBD: git rebase --interactive

 - TBD: Group commits by authors and title
   ```
   git shortlog
   ```

 - TBD: git merge

 - TBD: git lfs

 - TBD: git hooks

 - TBD: .gitconfig

 - TBD: `HEAD`, `FETCH_HEAD`, `ORIG_HEAD`, `MERGE_HEAD`, `CHERRY_PICK_HEAD`

 - TBD: git-send-email

 - TBD: CPSS upgrade

   Our SW relies on SW libraries from other SW companies, for example:
   CPSS, BCM SDK, OpenClovis, MetaSwitch, etc.

 - TBD: Git ignore `.gitignore`

 - TBD: TASK-UPDATE
   Update local GIT from remote GIT in case of part of files are locally (in work space) updated.

 - TBD: TASK-UPDATE
   Update local GIT from remote GIT in case of part of files are locally (in work space) updated, and remove all locally updated file.

 - TBD: TASK-PUSH-EXT
   Make “push” from local GIT to remote GIT in case of part of files are updated in remote GIT.

 - TBD: TASK-DIFF
   Make “diff” between local GIT and remote GIT in case of part of files are updated in local and in  remote GIT.

 - TBD: TASK-MERGE
   To work in the same time on some (2 or 3) different problems in local work-space
   (the same remote branch) and make separate commits into remote GIT.

 - TBD: TASK-MERGE
   To work in the same time on some (2 or 3) different problems in local work-space
   (the same remote branch) and make ONE commit into remote GIT for all fixes together.

 - TBD: TASK-MERGE
   To move some commits from one branch to other branch and commit into remote GIT.

 - TBD: TASK-DISCARD (CANCEL)
   To cancel some commits from local GIT (last or not-last).

 - TBD: git repo tool

 - TBD: git wtf

Git nuclear power commands
==========================

Git history of many (or all) commits may be rewritten in various ways:
 - Deleting specific files or directories in every commit
 - Changing commiter name or email
 - Editing commit messages of many commits (e.g., additing prefix to commit title)

For example, to add prefix for every commit title in a given range,
`git filter-branch --msg-filter` should be used in the below way:

```
git filter-branch --msg-filter 'echo "VTCAM: \c" && cat' 4dae947..HEAD
```

Or other variants of the command may be used:
```
git filter-branch --msg-filter 'echo "VTCAM: \c" && cat' HEAD~1..HEAD
git filter-branch --force --msg-filter 'echo "VTCAM: \c" && cat' HEAD~1..HEAD
```

Notes:
 - Reference: https://davidwalsh.name/update-git-commit-messages
