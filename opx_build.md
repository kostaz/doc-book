Performing full OPX build
=========================

```
cd main
./build_opx.py -f -v # full build with verbose output
```

Where flag `-v` (or `--verbose`) used for verbose output.

The flag `-f` (or `--full_build`) builds the following projects:

| Project          | Flag                               | Script                   |
|------------------|------------------------------------|--------------------------|
| main             | controlled by flag `main_build`    | in script `build_opx.py` |
| main-dev         | controlled by flag `main_dev_build`| in script `build_opx.py` |
| upgrade-director | controlled by flag `ud_build`      | in script `build_opx.py` |
| linux kernel     | controlled by flag `build_kernel`  | in script `build_opx.py` |

Also, the flag `main_build_all` is set to true which runs `do_make.sh build all`
from `main/platform/open_clovis/OP9500/scripts` directory.

Also, before the build is started the following files and directories are removed:
 - `tmp_local_build/root-fs-extras` (directory)
 - `tmp_local_build/cc_config.tgz`
 - `tmp_local_build/deploy-cc.tgz`
 - `tmp_local_build/deploy-lc.tgz`
 - `tmp_local_build/kernel.tgz`
 - `tmp_local_build/log.tgz`
 - `tmp_local_build/package.tgz`
 - `tmp_local_build/root-fs-extras.tgz`
 - `tmp_local_build/root-fs-overlay.tgz`
 - `tmp_local_build/root-fs-x86.tgz`

Also, the flag `unpack_fs` is set to false. It **prevents** execution of
`unpack.sh` script (`main_dev/dist/target_x86/scripts/unpack.sh`)
that removes RootFS directories for CC and LC cards (if the flag
`SKIP_BACKUP` is set to any value):
 - `~/root-fs-x86`
 - `~/root-fs-x86-lc`

Further, `build_opx.py` script verifies that `main`, `main_dev` and
`upgrade_director` projects exist before attempting compilation:
```
    # If unpacking, we have to build kernel
    for obj in must_exits_repo_dir_list:
        obj.__exec__(verbose)
```

Further, `build_opx.py` script performs the build of the desired
projects according to set variables:
```
    # Build all
    if (main_dev_build == True):
        build_main_dev()
    if (main_build == True):
        build_main()
    if (ud_build == True):
        build_ud()
```

The end of the script.


Function `build_main_dev()`
==========================

```
cd main_dev/dist/target_x86/scripts
export MRV_VERSION=...
sudo rm -rf ../../boot_src/linux
sudo ./build_kernel.sh

main_dev/dist/target_x86/compressed
copy root_fs.tar.gz to tmp_local_build/root-fs-x86.tgz
copy log.tgz        to tmp_local_build/root-fs-x86.tgz

remove tmp_local_build/root-fs-extras directory
copy main_dev/dist/target_x86/extras directory to ./root-fs-extras
```




Meaning of main variables
=========================

Variable `main_build_all`
=========================
Runs the script `do_make.sh` from directory `platform/open_clovis/OP9500/scripts`:
```
cd platform/open_clovis/OP9500/scripts
./do_make.sh build all
```
