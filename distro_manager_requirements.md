Note:
====
DM - Distribution Manager


DM requirements list
====================
- Support for git to keep DM source in git and track DM changes
- Compile, clean and install each distribution package distinctly
- Verify there is no conflicts between installed packages (e.g., ip util
  comes only from one package `iputils`)
- Support for git - all source code of all packages and DM should be
  kept in git
- Support for debug - each package may be compiled and installed
  with debug support
- Support to easily enable or disable a wide range of utilities
- Support for BusyBox
- Support for all-hands version
    - Save version of every package (including proprietary apps)
      in a readable form to enable version tracking of between releases.
    - Support for producing "what changes" matrix for QA
    - Version is git commit id paired with humanly-readable version and
      product release version and YYYY/MM/DD style, e.g.:
      U-Boot 4.7.1.144 2017/02/21 84a72d0
- Support for TeamCity and build tags (but without duplicated git tags)
    - Or other CI infrastructure
- Support for Marvell toolchain (proprietory toolchain)
- Support for installing Linux Kernel headers into toolchain
- Support for producing RootFS as UBIFS and Ubinized images
- Support for adding external packages that are not pre-included in DM
- Support for dependancy between packages (e.g, if `router` is changed,
  then other MRV user apps should be recompiled automatically)
- Support for product family
    - Similar products may be compiled fast
- Fast build time
- Provide security fixes for all supported packages
- Widely supported by the industry
- Support for simulation (QEMU or alike)
- Support for SDN/NFV (should be elaborated here)
- Support for ARM and x86 (in case of SDN/NFV router packages should be
  capable of running on x86)
- Support for producing SDK, so, that user application developers will
  not be mandated to build the distribution from scratch
- Self-contained DM - not dependent on Host PC libraries and packages
- Support for other than Marvell toolchains (for comparison and debug)
- Support for `make menuconfig` of every KBuild-based package
- Support for `/dev` management using devtmpfs and mdev
- Support for various system initializations (busybox, systemV, systemd)
- Support for offline build (so that all source code is saved at MRV machines)
    - 1) Download all sources
    - 2) Then build all
- Support for third-party escrow
- Producing dependency graph between packages
- Support for integration with Eclipse
- Support for cross debug with gdb
- Support for producing md5sum-identical builds on different machines
- Support for keeping **all** the source code of packages in git
  without any local patches
- Proper documentation
- Support for **not** using `sudo` root permissions during build and install
- Support to add custom user accounts for RootFS
- Compliance with various open-source licenses
- Support for producing USB live boot images
- Support for profiling and benchmark utils
- Support for debug utils (e.g. valgrind)
- Support for python on target
- Support for external kernel modules
- Support for production environment (should be elaborated)
- Support for reference distribution for Marvell-based products


Candidates for DM
=================
- Buildroot
- Yocto
- OpenWRT
- Arch Linux ARM
