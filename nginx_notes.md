NGINX notes
===========

##### Reference:
https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-16-04

Installation and management
===========================

```
sudo apt install nginx nginx-doc
systemctl status nginx
```

In Web Browser enter:
```
http://172.21.10.180
```

Manage NGINX process:
```
systemctl status nginx
sudo systemctl stop nginx
sudo systemctl start nginx
sudo systemctl restart nginx
```

To reload configuration _without_ dropping connections:
```
sudo systemctl reload nginx
```

To enable or disable Nginx auto-start at boot:
```
sudo systemctl enable nginx
sudo systemctl disable nginx
```

Important Nginx Files and Directories
=====================================



```
 1 <!DOCTYPE html>
 2 <html>
 3 <head>
 4 <title>Welcome to nginx!</title>
 5 <style>
 6     body {
 7         width: 35em;
 8         margin: 0 auto;
 9         font-family: Tahoma, Verdana, Arial, sans-serif;
10     }
11 </style>
12 </head>
13 <body>
14 <h1>Welcome to nginx!</h1>
15 <p>If you see this page, the nginx web server is successfully installed and
16 working. Further configuration is required.</p>
17
18 <p>For online documentation and support please refer to
19 <a href="http://nginx.org/">nginx.org</a>.<br/>
20 Commercial support is available at
21 <a href="http://nginx.com/">nginx.com</a>.</p>
22
23 <p><em>Thank you for using nginx.</em></p>
24 </body>
25 </html>
```

```
 1 4 -rw-r--r-- 1 root root 1077 Feb 11  2017 ./fastcgi.conf
 2 4 -rw-r--r-- 1 root root 1007 Feb 11  2017 ./fastcgi_params
 3 4 -rw-r--r-- 1 root root 2837 Feb 11  2017 ./koi-utf
 4 4 -rw-r--r-- 1 root root 2223 Feb 11  2017 ./koi-win
 5 4 -rw-r--r-- 1 root root 3957 Feb 11  2017 ./mime.types
 6 4 -rw-r--r-- 1 root root 1462 Feb 11  2017 ./nginx.conf
 7 4 -rw-r--r-- 1 root root  180 Feb 11  2017 ./proxy_params
 8 4 -rw-r--r-- 1 root root  636 Feb 11  2017 ./scgi_params
 9 4 -rw-r--r-- 1 root root 2074 Feb 11  2017 ./sites-available/default
10 0 lrwxrwxrwx 1 root root   34 May  3 11:38 ./sites-enabled/default -> /e
11 4 -rw-r--r-- 1 root root  422 Feb 11  2017 ./snippets/fastcgi-php.conf
12 4 -rw-r--r-- 1 root root  217 Feb 11  2017 ./snippets/snakeoil.conf
13 4 -rw-r--r-- 1 root root  664 Feb 11  2017 ./uwsgi_params
14 4 -rw-r--r-- 1 root root 3071 Feb 11  2017 ./win-utf
```



Ubuntu 18.04 LTS   : Bionic Beaver
Ubuntu 17.10       : Artful Aardvark
Ubuntu 16.04.X LTS : Xenial Xerus
Ubuntu 14.04.X LTS : Trusty Tahr

https://nodejs.org/en/download/package-manager/

```
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
```
