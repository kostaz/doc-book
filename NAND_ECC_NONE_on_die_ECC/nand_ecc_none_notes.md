##### Flash type
Macronix Micron NAND flash HW ECC MX30LF1GE8AB 3V 1Gb v1.3
Log from Linux Kernel boot:
```
NAND device: Manufacturer ID: 0x2c, Chip ID: 0xdc (Micron NAND 512MB 3,3V 8-bit)
NAND device has on die ECC
```


##### Start `minicom` with log file:
```
mkdir ~/dev/tmp/minicom
sudo minicom --wrap --baudrate 9600 --device /dev/ttyS0 \
    -C ~/dev/tmp/minicom/log_`date +"%Y%m%d_%H%M%S"`
```

##### Remount examples:
```
mount -o remount,sync /tmp/backup/
mount -o remount,async /tmp/backup/
```

##### Loop-test untar of ver file:
```
#!/bin/bash

COUNTER=0
while [  $COUNTER -lt 1000 ]; do
  echo "$COUNTER: tar vxzf OS940-1_5_9.19351-os606.ver -C /ver_unzipped/ 1>/dev/null"
  tar vxzf OS940-1_5_9.19351-os606.ver -C /ver_unzipped/ 1>/dev/null
  let COUNTER=COUNTER+1
done
```

##### Burn U-Boot and Master-OS:
```
protect off all ; nand erase ; setenv ipaddr 172.21.10.174 ; setenv serverip 172.21.10.48; bubt u-boot-kw_3_5_2_5.bin ; nand erase 00100000 0ff00000 ; wrTFTP2flash kernel.jffs2 08100000 00100000 ; wrTFTP2flash root.jffs2 00400000 08400000
```

##### U-Boot: Configure NFS boot:
```
setenv ipaddr 172.21.10.174
setenv serverip 172.21.10.179
setenv bootcmd 'run nfsboot'
setenv rootpath /home/yocto/dev/kw_nand_ecc_none/build_kw_2.6.22/dist/arm_kw_root
saveenv
```


##### Short copy example:
```
umount /tmp/backup

flash_erase --jffs2 /dev/mtd1 0 0 1>/dev/null
flash_erase --jffs2 /dev/mtd5 0 0 1>/dev/null

mkdir /tmp/mtdblock1
mkdir /tmp/mtdblock5

mount /dev/mtdblock1 /tmp/mtdblock1
mount /dev/mtdblock5 /tmp/mtdblock5

cp /big_file.tar.gz /tmp/mtdblock1
```
