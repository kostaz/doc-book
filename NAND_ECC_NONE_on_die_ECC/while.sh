#!/bin/bash

COUNTER=0
while [  $COUNTER -lt 1000 ]; do
  echo "$COUNTER: tar vxzf OS940-1_5_9.19351-os606.ver -C /ver_unzipped/ 1>/dev/null"
  tar vxzf OS940-1_5_9.19351-os606.ver -C /ver_unzipped/ 1>/dev/null
  let COUNTER=COUNTER+1
done
