Notes to build vanilla CPSS appDemo
===================================

### Clone CPSS git from BitBucket:
```
git clone ssh://git@bitbucket.mrv.co.il:7999/osv2018/cpss.git
cd cpss
```

### To build Marvell vanilla `appDemo` CPSS 2018.2
```
git checkout -b cpss-4.2.2018.2-vanilla "Marvell Release CPSS-Build_4.2_250-Ver_4.2.2018.2"
git checkout -b cpss-4.2.2018.2-vanilla c90fccb

git log --oneline --decorate --max-count=10
c90fccb (HEAD -> cpss-4.2.2018.2-vanilla, origin/marvell_releases) Marvell Release CPSS-Build_4.2_250-Ver_4.2.2018.2
9af3854 Marvell Release CPSS-Build_4.2_204-Ver_4.2.2017.12
7901bd1 Marvell Release CPSS-Build_4.2_170-Ver_4.2.2017.10
9c8d387 Marvell Release CPSS-Build_4.2_009-Ver_4.2.2017.9
8e5a509 Marvell Release CPSS-Build_4.2_124-Ver_4.2.2017.8
5574c9e Marvell Release CPSS-Build_4.2_099-Ver_4.2.2017.7
63cdb9c Marvell Release CPSS-Build_4.2_074-Ver_4.2.2017.5
5f6ad63 Marvell Release CPSS-Build_4.2_037-Ver_4.2.2017.3
a78bde4 Marvell Release CPSS-Build_4.2_012-Ver_4.2.2017.2

export PATH=$PATH:~/My/ADVA/REPO-PKG-2018.2/Build/msys_unified/dist/build-gcc-4.8.5-glibc-2.19-cpu-_msys/toolchain/bin
time ./build_cpss.sh MSYS_3_10 DX_ALL UTF_NO CUST CPSS_ENABLER_SHARED NOKERNEL
```

### To build MRV-based `appDemo`
```
git clone ssh://git@bitbucket.mrv.co.il:7999/osv2018/cpss.git
cd cpss

git log --oneline --decorate --max-count=10
381b768 (HEAD -> cpss-2018.2-master, origin/work_rebase_on_2018.2, origin/cpss-2018.2-master, origin/XG312-W26-18, origin/Sprint-W26-18) Add XG312, Step-2: Tracing for INIT
f9baee3 Add XG312, Step-1
d16faa1 Compile warning messages fix
c6bcb90 (tag: Merge-CPSS) Changed: debug log msgs with -if X312_Debug- condition
d3b716e Some interrupts: MMPCS_.., PCS_ALIGN_LOCK_LOST, CNC_..  masked
d516336 Add file for compatibility with 4.2.2017
3a823fb Merging and compilation issues fix
a00a709 Added -typedef struct- for compatibility with Marvell version 4.2.2017.7
0be7806 Fix: Frame Generator mode configuration for Tx-Sdma Queues
1b10fe9 Fix compilation issues, device is UP on the base CPSS-2017.7

export PATH=$PATH:~/My/ADVA/REPO-PKG-2018.2/Build/msys_unified/dist/build-gcc-4.8.5-glibc-2.19-cpu-_msys/toolchain/bin
time ./build_cpss.sh MSYS_3_10 DX_ALL UTF_NO CUST CPSS_ENABLER_SHARED NOKERNEL
```


### Verify `appDemo` is created
```
ls -ls ./compilation_root/appDemo
file ./compilation_root/appDemo
```
Example:
```
ls -ls ./compilation_root/appDemo
10388 -rwxrwxr-x 1 pavel pavel 10633492 Jun 28 16:21 ./compilation_root/appDemo
➜  /home/pavel/KostaTemp/appDemo-2018.2/cpss git:(cpss-4.2.2018.2-vanilla)

file ./compilation_root/appDemo
./compilation_root/appDemo: ELF 32-bit LSB executable, ARM, EABI5 version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.3, for GNU/Linux 2.6.16, BuildID[sha1]=fe538fe1bec822d8c368af0014161e63d3ca38f1, not stripped
```

### In U-Boot on device
```
Marvell>> setenv bootmode linux
Marvell>> saveenv
```

### Copy `appDemo` to NFS rootfs
Example:
```
to_dir=/home/pavel/My/ADVA/REPO-PKG-2018.2/Build/msys_unified/dist/arm_msys_root/appDemo_test

sudo mkdir $to_dir
sudo cp compilation_root/appDemo      $to_dir
sudo cp compilation_root/libcpss.so   $to_dir
sudo cp compilation_root/libhelper.so $to_dir

ls -ls $to_dir
```

### Boot device in Linux mode
```
Marvell>> boot
```

### Login as root
```
MRV Distribution for KW 88F6281.
OS-V8 login: root
Password:

#
```

### Enable Linux Kernel `dmesg`
```
# dmesg -n8
```

### Run `appDemo` on device
```
cd /appDemo_test
ls -ls
export LD_LIBRARY_PATH=.
./appDemo
```

Example `appDemo` initialization log:
```
# cd /appDemo_test
# ls -ls
# export LD_LIBRARY_PATH=.
# ./appDemo
mvPP opened
Unknown ioctl (36)
commander: threads are running: luaCLI cmdShell

Failed to get config.txt.194.90.136.70 from 172.21.10.146: timeout waiting ack
Failed to get config.txt from 172.21.10.146: timeout waiting ack

Supported boards:
+---------------------------------+-----------------------------------------+
| Board name                      | Revision                                |
+---------------------------------+-----------------------------------------+
| 19 - xCat, xCat2, xCat3         |                                         |
|                                 | 01 - Rev 0.1                            |
|                                 | 02 - Rev 0.2 - SDMA                     |
|                                 | 03 - Rev 0.3 - FC OFF                   |
|                                 | 04 - Rev 0.4 - PBR                      |
|                                 | 05 - Rev 0.5 - P24/25 SGMII mode, p26/27|
|                                 |      QX mode                            |
|                                 | 06 - Rev 0.6 - PBR SDMA                 |
|                                 | 07 - Rev 0.7 - SMI only board           |
|                                 | 08 - Rev 0.8 - Native MII               |
|                                 | 09 - Rev 0.9 - PHYMAC                   |
|                                 | 10 - Rev 0.10 - Legacy VPLS Enabled     |
|                                 | 11 - Rev 0.11 - +Linux BM kernel driver |
+---------------------------------+-----------------------------------------+
| 27 - Lion2                      |                                         |
|                                 | 01 - Rev 0.1 - 96X10G_SR_LR             |
|                                 | 02 - Rev 0.2 - 32X40G_SR_LR             |
|                                 | 03 - Rev 0.3 - 96X10G_KR                |
|                                 | 04 - Rev 0.4 - 32X40G_KR4               |
|                                 | 05 - Rev 0.5 - CT 10G 40G loopback Port |
|                                 | 06 - Rev 0.6 - 56X10G 210MHz            |
|                                 | 07 - HooperRD:0..7-10G_KR;8...-40G_KR   |
|                                 | 08 - HooperRD:0..7-10G_KR;8...-40G_KR:  |
|                                 |      kernel driver                      |
|                                 | 10 - Rev 0.10- MII interface            |
|                                 | 11 - 96X10G_SR_LR, PFC enable TC5_6 all |
|                                 |      ports                              |
|                                 | 12 - 96X10G_SR_LR, Lion2-4 cores        |
|                                 | 22 - multi port group FDB lookup        |
|                                 | 24 - multi port group FDB lookup : 256K |
|                                 |      mode - auto init                   |
|                                 | 25 - multi port group FDB lookup : 128K |
|                                 |      mode - auto init                   |
|                                 | 26 - 360MHz 0,1,2,3x10G 4,8,9x40G       |
+---------------------------------+-----------------------------------------+
| 29 - Bobcat2, Caelum, Cetus,    |                                         |
| Cygnus, Lewis, Aldrin, Bobcat3, | 01 - All Devices Regular init, No TM    |
| Aldrin2                         | 02 - Bobcat2, Caelum, Cetus - TM enabled|
|                                 | 03 - Bobcat2 MTL RD board 48GE          |
|                                 |      +4X10G/Aldrin MTL RD board 24*10G  |
|                                 |      +2X40G                             |
|                                 | 04 - Bobcat2 MTL RD board 4*10G         |
|                                 |      +2X40G->10G TM Enable              |
|                                 | 05 - All Devices Simplified Init DB     |
|                                 |      board                              |
|                                 | 06 - Bobcat2, Caelum, Cetus Simplified  |
|                                 |      Init DB board - TM Enabled         |
|                                 | 11 - Bobcat2 RD Board                   |
|                                 | 12 - Bobcat2 RD Board TM Enabled        |
+---------------------------------+-----------------------------------------+
| 30 - Six BobCat2                |                                         |
|                                 | 01 - Rev 0.1                            |
+---------------------------------+-----------------------------------------+
| 31 - Two Lion2 + Bobcat2        |                                         |
|                                 | 01 - Rev 0.1 - only Two Lion2           |
|                                 | 02 - Rev 0.2                            |
|                                 | 03 - Rev 0.3 - Two Hoopers + Bobcat2    |
|                                 | 04 - Rev 0.4 - One Hooper  + Bobcat2    |
+---------------------------------+-----------------------------------------+
| 32 - AC3x                       |                                         |
|                                 | 01 - 24/48*1G + 4*10G + 2*40G           |
|                                 |      (3/6*PHY1690)                      |
|                                 | 02 - 24*2.5G  + 4*10G + 1*40G           |
|                                 |      (no_PHY1690)                       |
|                                 | 03 - 32*1G  + 16*2.5G + 1*40G           |
|                                 |      (4*PHY1690)                        |
|                                 | 04 - 24/48*1G  + 4*10G + 2*40G          |
|                                 |      (3/6*PHY1690) - link state polling |
|                                 |      task                               |
+---------------------------------+-----------------------------------------+
| 33 - PX-Pipe                    |                                         |
|                                 | 01 - 12*10G + 100G                      |
+---------------------------------+-----------------------------------------+
| 34 - PX-Pipe * 4 (RD board)     |                                         |
|                                 | 01 - 4 devs each 12*10G + 4*25G         |
+---------------------------------+-----------------------------------------+

Call cpssInitSystem(index,boardRevId,reloadEeprom), where:
        index        - The index of the system to be initialized.
        boardRevId   - The index of the board revision.
        reloadEeprom - Whether the device's eeprom should be reloaded
                         after start-init.

Cpss DxCh version: CPSS 4.2.2018.2
CPSS Version Stream: CPSS_4.2_250

***************************************
 Prestera commander shell server ready
***************************************

LUA_CLI with examples
Entering LuaCLI took 3938.051640 msec

 LUA CLI based on LUA 5.1 from www.lua.org
 LUA CLI uses Mini-XML engine from www.minixml.org
***************************************************
               LUA CLI shell ready
***************************************************

Console#
```



```
./build_cpss.sh MSYS_3_10 DX_ALL UTF_NO UNZIP CPSS_ENABLER_SHARED NOKERNEL
```





OTHER UNCHECKED INFORMATION
===========================
linux root shell

appDemo

CLIexit

@@appDemoDbEntryAdd "bc2BoardType",0x50

luaShellExecute appDemoDbEntryAdd "bc2BoardType",0x50


shell-execute appDemoDbEntryAdd "bc2BoardType",0x50



shell-execute osPrintSyncEnable 1


Console# cpssInitSystem 29,1 noPorts apEnable



Console# configure
Console(config)# interface range ethernet 0/67
Console(config-if)# ap-port fc_pause false fc_asm_dir symmetric fec_supported false fec_required false lane_num 0 mode1 KR speed1 10000


do shell-execute mvApPortCtrlDebugInfoShow 255,255
do shell-execute mvHwsAvagoSerdesPMDdebugPrint 0 32












final:
======
appDemo
shell-execute appDemoDbEntryAdd "bc2BoardType",0x50
shell-execute osPrintSyncEnable 1
Console# cpssInitSystem 29,1 noPorts apEnable

ap-port fc_pause false fc_asm_dir symmetric fec_supported disabled fec_required disabled lane_num 0 mode1 KR speed1 10000

do shell-execute mvApPortCtrlDebugInfoShow 255,255
do shell-execute mvHwsAvagoSerdesPMDdebugPrint 0 32

Please do the following:
1.       Cetus to Intel – Set the AP port to intel and provide the following:
a.       do shell-execute mvApPortCtrlDebugInfoShow…. as you did
b.      do shell-execute mvHwsAvagoSerdesPMDdebugPrint …. (see below)
2.       Cetus to Cetus - Try the Cetus to Cetus configuration and provide the log
a.       do shell-execute mvApPortCtrlDebugInfoShow… as you did
b.      do shell-execute mvHwsAvagoSerdesPMDdebugPrint …. (see below)
c.       do shell-execute mvDxChPortSerdesManualTxConfigGet …. (see below)
3.       Try the EoM API with this version and let me know if that works for you

Yotam:
======
appDemo
shell-execute appDemoDbEntryAdd "bc2BoardType",0x50
shell-execute osPrintSyncEnable 1
Console# cpssInitSystem 29,1 noPorts apEnable

configure
interface range ethernet 0/56-57



speed 10000 mode KR
prbs enable lane all mode 31
prbs show lane all

eom matrix port 0/56 serdes 2 min-dwell 1000000 max-dwell 100000000 noeye



The Eye Monitor tool provides a key metric of SERDES channel quality. This document describes the functions and
options for capturing an eye using the Eye Monitor tool. It does not describe eye measurement theory or all the details
on when and why one might use this tool.

