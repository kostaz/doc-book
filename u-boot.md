
To boot from flash but with pre-defined OOB IP address
======================================================
```
Marvell>> printenv ramboot
ramboot=save; ubi part kernel1 4096; ubifsmount kernel; ubifsload 2000000 $image_name; ubi part dtb1 4096; ubifsmount dtb; ubifsload $fdtaddr $dtb_image_name;
 setenv bootargs $console $nandEcc $mtdparts ubi.mtd=3 root=ubi0:root rw rootfstype=ubifs ubi.mtd=2; bootm $loadaddr - $fdtaddr

Marvell>> printenv nfsboot
nfsboot=save; nfs 2000000 $rootpath/$image_name; nfs $fdtaddr $rootpath/$dtb_image_name; setenv bootargs  $console $nandEcc $mtdparts ubi.mtd=4 ubi.mtd=2 $boo
targs_root rootdelay=5 nfsroot=$serverip:$rootpath ip=$ipaddr:$serverip:$gatewayip:$netmask:MSYS-BC2:$netdev:none;bootm $loadaddr - $fdtaddr
```


Add IP address to `bootargs` variable:
`ip=1.1.1.100:1.1.1.200:1.1.1.254:255.255.255.0:MRV-BC2:eth0:none`

```
Marvell>> setenv ramboot_ip 'save; ubi part kernel1 4096; ubifsmount kernel; ubifsload 2000000 $image_name; ubi part dtb1 4096; ubifsmount dtb; ubifsload $fdtaddr $dtb_image_name; setenv bootargs $console $nandEcc $mtdparts ubi.mtd=3 root=ubi0:root rw rootfstype=ubifs ubi.mtd=2 rootdelay=5 ip=1.1.1.100:1.1.1.200:1.1.1.254:255.255.255.0:MRV-BC2:eth0:none; bootm $loadaddr - $fdtaddr'
```

And, of course, update `bootcmd` and save U-Boot environment:

```
Marvell>> setenv bootcmd 'run ramboot_ip'
Marvell>> save
```
