Notes to upgrade CPSS
=====================


### dos2unix
Need to be done for every CPSS vanilla commit!

Important note - the "dos2unix" commands should be done **inside** the `cpss/cpss/` directory!!
Otherwise, files from the `.git` directory may be changed!!

The updated command - all ASCII and Unicode files need to be fixed:
```
find . -type f | sort | xargs file | grep ASCII | cut -d':' -f1 | xargs dos2unix
find . -type f | sort | xargs file | grep Unicode | cut -d':' -f1 | xargs dos2unix
```


### gmake2.exe
Need to be done for every CPSS vanilla commit!

The Windows-only file is not needed:
```
rm cpss/tools/bin/gmake2.exe
```


### chmod -x for *.c and *.h files
After creating **all** CPSS vanilla new commits - do `git filter-branch` once.

User git nuclear power command:
```
git filter-branch --force --tree-filter 'find . -name "*.[ch]" -exec chmod -x {} \;' HEAD
```
