Open Agema bundle manually
==========================
```
export bundle=5.5.1.7901.bundle
export BOX_TYPE=WhiteBox
ARCHIVE=`awk '/^__ARCHIVE_BELOW__/ {print NR + 1; exit 0; }' $bundle`
export ARCHIVE
export bundle_dir="${bundle}_dir"

echo "==============="
echo $bundle
echo $BOX_TYPE
echo $ARCHIVE
echo $bundle_dir
echo "==============="

mkdir $bundle_dir
tail -n+$ARCHIVE $bundle | tar -xpzm -C "$bundle_dir"
```
