i586-poky-linux-objdump-p/home/yocto/dev/build_qemu/tmp/work/i586-poky-linux/safplus/6.1-r0/packages-split/safplus/usr/prebuild/target/i586/linux-4.4.98/bin/safplus_amf

[        -> /usr/bin/[
ar       -> /usr/bin/x86_64-linux-gnu-ar
as       -> /usr/bin/x86_64-linux-gnu-as
awk      -> /usr/bin/gawk
basename -> /usr/bin/basename
bash     -> /bin/bash
bzip2    -> /bin/bzip2
cat      -> /bin/cat
chgrp    -> /bin/chgrp
chmod    -> /bin/chmod
chown    -> /bin/chown
chrpath  -> /usr/bin/chrpath
cmp      -> /usr/bin/cmp
cp       -> /bin/cp
cpio     -> /bin/cpio
cpp      -> /usr/bin/cpp-5
cut      -> /usr/bin/cut
date     -> /bin/date
dd       -> /bin/dd
diff     -> /usr/bin/diff
diffstat -> /usr/bin/diffstat
dirname  -> /usr/bin/dirname
du       -> /usr/bin/du
echo     -> /bin/echo
egrep    -> /bin/egrep
env      -> /usr/bin/env
expand   -> /usr/bin/expand
expr     -> /usr/bin/expr
false    -> /bin/false
fgrep    -> /bin/fgrep




i586-poky-linux-addr2line
i586-poky-linux-ar
i586-poky-linux-as
i586-poky-linux-c++
i586-poky-linux-c++filt
i586-poky-linux-cpp
i586-poky-linux-dwp
i586-poky-linux-elfedit
i586-poky-linux-g++
i586-poky-linux-gcc
i586-poky-linux-gcc-7.2.0
i586-poky-linux-gcc-ar
i586-poky-linux-gcc-nm
i586-poky-linux-gcc-ranlib
i586-poky-linux-gcov
i586-poky-linux-gcov-dump
i586-poky-linux-gcov-tool
i586-poky-linux-gprof
i586-poky-linux-ld
i586-poky-linux-ld.bfd
i586-poky-linux-ld.gold
i586-poky-linux-nm
i586-poky-linux-objcopy
i586-poky-linux-objdump
i586-poky-linux-ranlib
i586-poky-linux-readelf
i586-poky-linux-size
i586-poky-linux-strings
i586-poky-linux-strip


NOTE: i586-poky-linux-objdump -p /home/yocto/dev/build_qemu/tmp/work/i586-poky-linux/safplus/6.1-r0/packages-split/safplus/usr/prebuild/target/i586/linux-4.4.98/bin/safplus_msg

ERROR: QA Issue: /usr/prebuild/target/i586/linux-4.4.98/bin/safplus_msg
contained in package safplus requires libClAmsXdr.so, but no providers
found in RDEPENDS_safplus? [file-rdeps]


[+] [  ] Install Open Clovis binaries to Yocto
[-] [1w] Fix Open Clovis makefiles
[+] [  ] Install BCM SDK binaries to Yocto

Yocto eSDK tasks
================
[-] [3d] Create Yocto eSDK
[-] [3d] Install Open Clovis binaries to Yocto eSDK
[-] [3d] Install BCM SDK binaries to Yocto eSDK
[-] [2w] Build 'main' with Yocto eSDK


gcp 7a376a3
gcp

2ee4796 - Clear /proc/sys/net/ipv6/conf/IF/disable_ipv6 when create VMACs (14 minutes ago) <Quentin Armitage>
cda9d46 - Change VRRP state change notification (16 minutes ago) <Barak Adam>

d84f08edabd4f7bc308983e57a1c741adeed522a is the first bad commit
commit d84f08edabd4f7bc308983e57a1c741adeed522a
Author: Quentin Armitage <quentin@armitage.org.uk>
Date:   Sun Mar 12 16:12:17 2017 +0000

    Don't link to libraries not required by configuration

    Signed-off-by: Quentin Armitage <quentin@armitage.org.uk>

:100755 100755 a8542f6df51a2efb75542dbea06f73580ab17566 95b4c800b82ddd7cddb4eae3bd65ecb5f037b444 M	configure
:100644 100644 719338af559a31ea8f5f5a5b00e176547b0a3a4d d8464d197d4a7e1b6bf249f4ed73f9841d800e58 M	configure.ac


================================================================================

w-4/12 (ww 9)

# Done

[+] [3h] Check keepalived v1.4.1 (fails to compile), last ok v1.3.4
[-] [2h] Discuss CPSS upgrade approaches

# Todo

[-] [  ] Compare safplus binary files safplus vs current Open Clovis
[-] [  ] Add Open Clovis artifacts to RootFS
[-] [  ] Add Open Clovis artifacts to Yocto eSDK
[*] [6h] Add BCM SDK to RootFS
[-] [  ] Add BCM SDK to Yocto eSDK
[-] [  ] Short try to build `main` project

# Internal

[-] [2h] Read 7.6.4. Packaging
[-] [2h] Read 4.10.1. Including Static Library Files
[-] [2h] Read [Why the Yocto Project for my IoT Project][1]?

# Misc

[1](https://www.embedded.com/electronics-blogs/say-what-/4458600/Why-the-Yocto-Project-for-my-IoT-Project-)




================================================================================

sudo curl https://storage.googleapis.com/git-repo-downloads/repo > /bin/repo
sudo chmod a+x /bin/repo

================================================================================

repo init -u ssh://git@bitbucket.mrv.co.il:7999/misc/manifest.git
-- or --
repo init -u ssh://git@bitbucket.mrv.co.il:7999/misc/manifest.git -m mos_only.xml
-- or --
repo init -u ssh://git@bitbucket.mrv.co.il:7999/misc/manifest.git -m cpss_only.xml

================================================================================

repo sync

================================================================================

repo start new_great_feature
-- or --
repo forall -c git checkout -b new_great_feature bitbucket/new_great_feature

================================================================================

repo branch
repo status
-- or --
repo forall -c git branch
repo forall -c git status

================================================================================

... dev ...

================================================================================

repo status
repo diff
-- or --
repo forall -c git status
repo forall -c git diff

================================================================================

repo forall -c git add -u
repo forall -c git commit -m 'new_great_feature - test 1'
repo forall -c git push -u bitbucket new_great_feature:new_great_feature

================================================================================


cd poky
yocto-layer create foo
cd build
bitbake-layers add-layer ../poky/meta-foo

devtool create-workspace workspace
devtool build-image core-image-full-cmdline

devtool add https://github.com/axel-download-accelerator/axel
devtool edit-recipe axel
devtool build axel

devtool deploy-target root@192.168.7.2
devtool undeploy-target root@192.168.7.2

devtool finish axel ../poky/meta-foo
devtool reset axel
rm -rf workspace/sources/axel

vim poky/meta-foo/



