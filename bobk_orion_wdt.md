Pre-U-Boot (DDR training)
=========================
Watchdog is also enabled in pre-U-Boot environment during DDR training
algorithm execution. The watchdog timeout is defined here to ~20 seconds.
To verify that watchdog resets the board upon failed DDR training,
prevent DDR to be successfully configured.

For example, disable power or clock that is connected to DDR chips on
board - this ensures that DDR training will fail.

After DDR training is failed, the board will reset in (maximum) 20 seconds.
Now re-enable DDR clock and power and reboot the board (or power cycle).
The board will successfully reach the U-Boot shell prompt.

Note: To disable watchdog in pre-U-Boot environment, U-Boot recompilation
is needed.


U-Boot
======
During U-Boot execution - watchdog is disabled.
Verify that watchdog is enabled in U-Boot environment variable `wd`:
```
Marvell>> printenv wd
wd=enable
```
This variable enables watchdog daemon (user-space application) in Linux system.


Linux Kernel boot
=================
Verify that during Linux Kernel boot `orion_wdt` has been started and
the initial timieout is configured to 45 seconds:
```
usbcore: registered new interface driver usb-storage
i2c /dev entries driver
rtc-ds1307 0-0068: rtc core: registered ds1338 as rtc0
rtc-ds1307 0-0068: 56 bytes nvram
orion_wdt: Initial timeout 45 sec
cesadev_init(c067f6f8)
usbcore: registered new interface driver usbhid
usbhid: USB HID core driver
```
If `orion_wdt` is started as shown above, it means that Linux Kernel support
for watchdog is enabled.


Router (Linux user-space) boot
==============================
Verify that during system boot watchdog user-space daemon has been started:
```
mount: Mounting ubi1_0 on /config failed: No such device
Starting watchdog...
Starting watchdog... Done.
Restore configuration... done.
Mounting Configuration Directories ...
```


After full system boot
======================
Enter into Linux prompt.
Verify that watchdog device file is present in the system:
```
# ls -ls /dev/watchdog*
   0 crw-------    1 root     root      10, 130 Jan  1  1970 /dev/watchdog
   0 crw-------    1 root     root     251,   0 Jan  1  1970 /dev/watchdog0
```

Verify that user-space watchdog daemon has been started:
```
# ps aux | grep watchdog
 1057 root        276 S   watchdog -t 20 /dev/watchdog0
```

Kill user-space watchdog daemon and verify that boards reboots
in maximum 45 seconds.
```
# kill -9 1057
watchdog watchdog0: watchdog did not stop!
```


Disabling watchdog
==================
Verify that if `wd` U-Boot environment variable is set to `disable`, then
the user-space watchdog daemon does not start.
