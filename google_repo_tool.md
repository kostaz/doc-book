Build OptiSwitch-2018.2 with repo tool
======================================

### Install pre-requisites
```
sudo apt-get install -y curl
```

### Install repo tool
Google has created repo tool to manage multiple git repositories.
```
sudo su
sudo curl https://storage.googleapis.com/git-repo-downloads/repo > /bin/repo
sudo chmod a+x /bin/repo
exit
```

### Clone git repositories
```
mkdir -p ~/dev/2018.2_`date +"%Y_%m_%d"` # equals `mkdir 2018.2_2018_06_27`
cd 2018.2_2018_06_27 # this is an example
repo init -u ssh://git@bitbucket.mrv.co.il:7999/osv2018/manifest.git
repo sync
```

### Alias `repo forall -c`
Add the below function into `~/.bashrc` file and vefiry it works.
```
function rfa()
{
	term=$@
	cmd="git remote get-url --all origin && ${term}"
	repo forall -c $cmd
}
```
Use the development environment scripts (from `env` repository) to get it for free.
Usage examples:
```
rfa git branch
rfa git branch --all
rfa git checkout cpss-2018.2-master
```

### Checkout or create branches
```
cd distribution
git checkout -b my_feature origin/cpss-2018.2-master

cd os-v
git checkout -b my_feature origin/cpss-2018.2-master

cd cpss
git checkout -b my_feature origin/cpss-2018.2-master
```

### Build in a regular way
```
./setup.sh msys_unified.ini -i <build_dir_full_path>
./mkall.sh ...
```

Example:
```
./setup.sh msys_unified.ini -i `pwd`/../build_unified
cd ../build_unified/scripts
time ./mkall.sh -V4_8_20 -dOS-V -N -L ../../os-v/
```
