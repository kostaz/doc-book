safplus_6.1.bb (at meta-openclovis/recipes-extended/safplus/)

EXTRA_OECONF = " \
  --with-sdk-dir=${OC_SDK} \
  --with-safplus-build \
  --without-snmp-build \
  --prefix=${D}${prefix} \
  "

do_configure () {

    mkdir -p ${OC_SDK}/6.1/sdk
    mkdir -p ${OC_SDK}/6.1/buildtools/yocto
    mkdir -p ${OC_ASP_BUILD}
    cd ${OC_ASP_BUILD}

    TARGET=${TARGET_OS} \
    ARCH=${TARGET_ARCH} \
    MARCH=${TARGET_ARCH} \
    ${S}/configure ${EXTRA_OECONF}
}

build/tmp/work/core2-32-poky-linux/safplus/6.1-r0/safplus-6.1/src/SAFplus/configure \
--with-sdk-dir=build/tmp/work/core2-32-poky-linux/safplus/6.1-r0/clovis \
--with-safplus-build \
--without-snmp-build \
--prefix=build/tmp/work/core2-32-poky-linux/safplus/6.1-r0/image/usr \
--disable-static

In short
========
```
$ cd safplus-6.1
$ ./src/SAFplus/configure --with-sdk-dir=clovis \
                          --with-safplus-build \
                          --without-snmp-build \
                          --prefix=image/usr
                          --disable-static
```

Safplus `./configure` fails
===========================
Let's check the log file here:
`build/tmp/work/core2-32-poky-linux/safplus/6.1-r0/temp/log.do_configure.XXXXX`

```
checking for glib-2.0 2.2.0... safplus-6.1/src/SAFplus/configure: line 7525: pkg-config: command not found
configure: error:
*** GLIB >= 2.2.0 is required. The latest version of
*** GLIB is always available from ftp://ftp.gtk.org/.
WARNING: exit code 1 from a shell command.
```

Let's check if glib package was built:
```
bitbake -s | grep -i glib
```

So, the relevant results are:
```
glib-2.0                                          1:2.52.3-r0
glib-2.0-native                                   1:2.52.3-r0
nativesdk-glib-2.0                                1:2.52.3-r0
```

Glib version
============
Again, the current Yocto's branch is `rocko`, which is released around
October 2017. Just check release schedule over here:
https://wiki.yoctoproject.org/wiki/Releases

This is latest stable Yocto release with the below versions:
- Yocto Project Version : 2.5
- Poky Version          : 18.0
- BitBake branch        : 1.36

Let's grep for glib in `poky` directory...

Let's take at a look at the dependency list in `safplus` bitbake recipe...


Safplus dependencies
====================
File `meta-openclovis/recipes-extended/safplus/safplus_6.1.bb`
describes the recipe to build `safplus` package.

```
 1 DESCRIPTION = "The canonical example of init scripts"
 2 SECTION = "base"
 3 LICENSE = "GPLv2"
 4 LIC_FILES_CHKSUM = "file://COPYING;md5=94d55d512a9ba36caa9b7df079bae19f"
 5 PR = "r0"
 6
 7 DEPENDS = "sqlite3 glib-2.0 gdbm"
 8 RDEPENDS_${PN} = "sqlite3 glib-2.0 gdbm"
 9 RPROVIDES_${PN} += "libClUtils.so libmw.so libClDbal.so"
10
11 SRCREV = "${AUTOREV}"
12 SRC_URI = "git://github.com/OpenClovis/SAFplus-Availability-Scalability-Platform"
13
14 inherit autotools
```

Well, let's try to build some dependency for safplus package.
For example, `gdbm` package.
```
bitbake gdbm
```

After the check, this `gdbm` package was already successfully built at
some previous stage. So, let's do the same for other prerequise
packages that we can see from safplus recipe `safplus_6.1.bb`:
```
DEPENDS = "sqlite3 glib-2.0 gdbm"
RDEPENDS_${PN} = "sqlite3 glib-2.0 gdbm"
```

So, building `glib-2.0` with bitbake (`bitbake glib-2.0`) succeeds
quietly. But let's just go inside `glib-2.0` build directory: 
```
bitbake glib-2.0 -c devshell
```

So, the glib full directory is: `build/tmp/work/core2-32-poky-linux/glib-2.0/1_2.52.3-r0/glib-2.52.3`,
which means that the glib version in Yocto 2.4 Rocko is recent enough!

I just tried to build `pkgconfig` package. I was sure that it was
built before, but bitbake actually worked hard to build it and
reported that `pkgconfig-0.29.2` was compiled successfully.
