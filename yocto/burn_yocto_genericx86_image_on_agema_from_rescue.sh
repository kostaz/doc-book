#!/bin/bash

###########################
# Burn Yocto image on Agema
###########################

# ~~~~~~~~~~~~~~~~~~~~~
# Print Agema disk info
# ~~~~~~~~~~~~~~~~~~~~~
sdx=sda
sgdisk -p /dev/$sdx

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Remote Yocto machine (may change often)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ipaddr=172.21.10.162
user=kosta
yocto=/home/kosta/dev/20180922_yocto_fast

################################################################################
# Write RootFS image to disk
################################################################################

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# RootFS image name and directory
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
image_dir=$yocto/build/tmp/deploy/images/genericx86
image=core-image-full-cmdline-genericx86.ext4

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Copy image to local machine
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~
scp $user@$ipaddr:$image_dir/$image .
md5sum $image

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Write RootFS image to set A of disk partitions
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sdxN=sda5
dd if=$image of=/dev/$sdxN
e2fsck -f /dev/$sdxN
resize2fs /dev/$sdxN

################################################################################
# Write Linux Kernel image to disk
################################################################################

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Kernel image name and directory
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
image_dir=$yocto/build/tmp/deploy/images/genericx86
orig_image=bzImage
image=vmlinuz

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Copy image to local machine
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~
scp $user@$ipaddr:$image_dir/$orig_image $image
md5sum $image

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Write kernel image to set A of disk partitions
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sdxN=sda4
# dd if=/dev/zero of=/dev/$sdxN
mkfs.ext4 -F /dev/$sdxN
umount /tmp/$sdxN 2>/dev/null
mkdir -p /tmp/$sdxN
mount /dev/$sdxN /tmp/$sdxN
cp $image /tmp/$sdxN
ls -ls /tmp/$sdxN
umount /tmp/$sdxN

exit 0

# ~~~~~~~~~~~~~~~~~~~~~~~~~~
# Review USB disk for sanity
# ~~~~~~~~~~~~~~~~~~~~~~~~~~
sudo sgdisk -p /dev/$sdx
sudo fdisk -l /dev/$sdx

sudo umount /dev/${sdx}1 2>/dev/null
sudo umount /dev/${sdx}2 2>/dev/null
sudo umount /dev/${sdx}3 2>/dev/null
sudo umount /dev/${sdx}  2>/dev/null

dir=/tmp/${sdx}1 ; if [ ! -d $dir ] ; then mkdir $dir ; fi
dir=/tmp/${sdx}2 ; if [ ! -d $dir ] ; then mkdir $dir ; fi
dir=/tmp/${sdx}3 ; if [ ! -d $dir ] ; then mkdir $dir ; fi

sudo mount -t ext4 /dev/${sdx}1 /tmp/${sdx}1
sudo mount -t ext4 /dev/${sdx}2 /tmp/${sdx}2
sudo mount -t ext4 /dev/${sdx}3 /tmp/${sdx}3

mount | tail -n 3
