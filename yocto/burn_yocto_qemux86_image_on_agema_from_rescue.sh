#!/bin/bash

###########################
# Burn Yocto image on Agema
###########################

# ~~~~~~~~~~~~~~~~~~~~~
# Print Agema disk info
# ~~~~~~~~~~~~~~~~~~~~~
sdx=sda
sgdisk -p /dev/$sdx

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Remote Yocto machine (may change often)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ipaddr=172.21.10.162
user=kosta
yocto=/home/kosta/dev/20180917_yocto_fresh

################################################################################
# Write RootFS image to disk
################################################################################

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# RootFS image name and directory
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
image_dir=$yocto/build/tmp/deploy/images/qemux86
image=core-image-full-cmdline-qemux86.ext4

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Copy image to local machine
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~
scp $user@$ipaddr:$image_dir/$image .
md5sum $image

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Write RootFS image to set A of disk partitions
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sdxN=sda5
dd if=$image of=/dev/$sdxN
e2fsck -f /dev/$sdxN
resize2fs /dev/$sdxN

################################################################################
# Write Linux Kernel image to disk
################################################################################

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Kernel image name and directory
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
image_dir=$yocto/build/tmp/deploy/images/qemux86
orig_image=bzImage
image=vmlinuz

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Copy image to local machine
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~
scp $user@$ipaddr:$image_dir/$orig_image $image
md5sum $image

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Write kernel image to set A of disk partitions
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sdxN=sda4
# dd if=/dev/zero of=/dev/$sdxN
mkfs.ext4 -F /dev/$sdxN
umount /tmp/$sdxN 2>/dev/null
mkdir -p /tmp/$sdxN
mount -t ext4 /dev/$sdxN /tmp/$sdxN
cp $image /tmp/$sdxN
ls -ls /tmp/$sdxN
umount /tmp/$sdxN

exit 0
