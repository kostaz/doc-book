#!/bin/bash

#########################
# Burn Yocto image on USB
#########################

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Remote Yocto machine (may change often)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ipaddr=172.21.10.162
user=kosta
yocto=/home/kosta/dev/20180922_yocto_fast

# ~~~~~~~~~~~~~~~~~~~~~~~~
# Image name and directory
# ~~~~~~~~~~~~~~~~~~~~~~~~
image_dir=$yocto/build/tmp/deploy/images/genericx86
wic_image=core-image-full-cmdline-genericx86.wic
wic_bmap=core-image-full-cmdline-genericx86.wic.bmap

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Copy image to local machine
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~
scp $user@$ipaddr:$image_dir/$wic_bmap .
scp $user@$ipaddr:$image_dir/$wic_image .

# ~~~~~~~~~~~~~~~~~~~~~~~
# Unmount USB disk on key
# ~~~~~~~~~~~~~~~~~~~~~~~
sdx=sdc

sudo umount /dev/${sdx}1 2>/dev/null
sudo umount /dev/${sdx}2 2>/dev/null
sudo umount /dev/${sdx}3 2>/dev/null
sudo umount /dev/${sdx}  2>/dev/null

rm -rf /tmp/${sdx}1
rm -rf /tmp/${sdx}2
rm -rf /tmp/${sdx}3
rm -rf /tmp/${sdx}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# DANGER!!! Zero out all GPT and MBR data structures
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sudo sgdisk -z /dev/$sdx

# ~~~~~~~~~~~~~~~~~
# Prepare variables
# ~~~~~~~~~~~~~~~~~
sudo bmaptool copy $wic_image /dev/$sdx

exit 0

# ~~~~~~~~~~~~~~~~~~~~~~~~~~
# Review USB disk for sanity
# ~~~~~~~~~~~~~~~~~~~~~~~~~~
sudo sgdisk -p /dev/$sdx

sudo umount /dev/${sdx}1 2>/dev/null
sudo umount /dev/${sdx}2 2>/dev/null
sudo umount /dev/${sdx}3 2>/dev/null
sudo umount /dev/${sdx}  2>/dev/null

dir=/tmp/${sdx}1 ; if [ ! -d $dir ] ; then mkdir $dir ; fi
dir=/tmp/${sdx}2 ; if [ ! -d $dir ] ; then mkdir $dir ; fi
dir=/tmp/${sdx}3 ; if [ ! -d $dir ] ; then mkdir $dir ; fi

sudo mount -t ext4 /dev/${sdx}1 /tmp/${sdx}1
sudo mount -t ext4 /dev/${sdx}2 /tmp/${sdx}2
sudo mount -t ext4 /dev/${sdx}3 /tmp/${sdx}3

mount | tail -n 3
