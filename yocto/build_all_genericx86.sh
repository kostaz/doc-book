#!/bin/bash -x

build_start=$SECONDS

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Build Yocto core-image-full-cmdline
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
date ; time bitbake core-image-full-cmdline                     ; date
date ; time bitbake core-image-full-cmdline -c populate_sdk     ; date
date ; time bitbake core-image-full-cmdline -c populate_sdk_ext ; date

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Copy Yocto eSDK to "server" (so app devs take it in auto-fashion during setup)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
name=poky-glibc-x86_64-core-image-full-cmdline-core2-32-toolchain-ext-2.5.1.sh
scp tmp/deploy/sdk/$name.* mrv@172.21.9.223:~

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Build Yocto core-image-minimal
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
date ; time bitbake core-image-minimal                     ; date
date ; time bitbake core-image-minimal -c populate_sdk     ; date
date ; time bitbake core-image-minimal -c populate_sdk_ext ; date

# ~~~~~~~~~~~~
# Install eSDK
# ~~~~~~~~~~~~
esdk_dir=/opt/yocto/2.5.1/esdk
esdk_installer=tmp/deploy/sdk/$name.sh

rm -rf $esdk_dir
$esdk_installer -y -d $esdk_dir

# ~~~~~~~~~~~~~~~~
# Print build time
# ~~~~~~~~~~~~~~~~
secs=$(( SECONDS - build_start ))
h=$(($secs%86400/3600))
m=$(($secs%3600/60))
s=$(($secs%60))
echo "************************************"
printf 'build time %dh:%dm:%ds\n' $h $m $s
echo "************************************"
