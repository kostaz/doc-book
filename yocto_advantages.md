Why Yocto?
==========

Agenda
======

 - Yocto Project high-level design
 - Ecosystem of individuals and organizations
 - Shared development environment
 - Power of devtool
 - Improved team workflow
 - Leverages shared state for faster builds
 - Runs on a range of host operating systems
 - Runtime testing of images
 - Extra image configuration defaults (see local.conf)
 - Build history to track changes between builds
 - Shared-state files from other locations
 - Default built-in VNC server
 - The devshell per package
 - Pretty active IRC channel on freenode
 - Wealth of information on the web
 - Marvell latest SoC-s are release with Yocto and Buildroot support
 - Support for product upgrade to new Yocto release
       - All new packages, libraries and Linux Kernel

Excellent documentation
Wiki, Google
6-month release cycle
Package manager
E.g., provide Master-OS as a debian package
Fast consecutive builds
Also, shared build cache between developers
Security committee
All security patches inside
Each package can be build on its own
$ bitbake <package> -c devshell
Very important when debugging issues





Supports many architectures
x32, x86_64, ARMv7, etc.
Learn once, use everywhere
Reproducible builds
8,000 packages supported (Mostly the same packages as Buildroot)
meta-virtualization (Docker, KVM, LXC, Xen) and meta-openstack layers
Strong community
Linux Foundation project
All modern stuff is supported
GCC 7.x, Linux Kernel 4.12, latest stable packages
Support for application developers
Provides SDK


Yocto Project high-level design
===============================
 - 






Runtime testing of images
-------------------------
The build system can test booting virtual machine images under qemu (an emulator)
after any root filesystems are created and run tests against those images. To
enable this uncomment this line. See classes/testimage(-auto).bbclass for
further details.
TEST_IMAGE = "1"


Extra image configuration defaults
----------------------------------


Build history to track changes between builds
---------------------------------------------


Shared-state files from other locations
---------------------------------------
As mentioned above, shared state files are prebuilt cache data objects which can
used to accelerate build time. This variable can be used to configure the system
to search other mirror locations for these objects before it builds the data itself.
SSTATE_MIRRORS...


Default built-in VNC server
---------------------------


The devshell per recipe
-----------------------
