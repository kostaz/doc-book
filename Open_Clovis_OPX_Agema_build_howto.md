Intro
=====
The installation procedure was tested on Ubuntu 16.04 64-bit native.
Native Ubuntu - meaning "not virtual machine".


Configure git
=============
Configure git for **both** users - the regular user **and** `root`.
See the example below.

#### For regular user:
```
$ git config --global user.name "User Name"
$ git config --global user.email "username@mrv.com"
```

#### For `root` user:
```
$ sudo su
$ git config --global user.name "User Name"
$ git config --global user.email "username@mrv.com"
```


SSH Keys
========
1) Generate SSH keys for the regular and `root` users.
2) Add newly-generated SSH keys to your BitBucket account.

#### For regular user:
- Generate SSH keys using `ssh-keygen` util:
  ```
  $ ssh-keygen
  ```
- Copy SSH keys from `~/.ssh/id_rsa.pub` file to clipboard
- Go to "manage account" on bitbucket and add those keys by "add" and paste

#### For `root` user:
- Generate SSH keys using `ssh-keygen` util:
  ```
  $ sudo su
  $ ssh-keygen
  ```
- Copy SSH keys from `~/.ssh/id_rsa.pub` file to clipboard
- Go to "manage account" on bitbucket and add those keys by "add" and paste


The right way
=============
This is relateviley long procedure to setup Agema
development environment. It takes about two-three
hours. Do not rush it. Do every step carefully.

```
$ sudo apt-get install git
$ git clone ssh://git@bitbucket.mrv.co.il:7999/misc/env.git
$ cd env
```
Follow the instructions in `README_whitebox.md` file carefully.


Verify Access to BitBucket
==========================
Verify that `git clone` works ok from regular user and `root` account.
#### For regular user:
```
$ cd /tmp
$ git clone ssh://git@bitbucket.mrv.co.il:7999/misc/doc-book.git
$ rm -rf doc-book
```
#### For `root` user:
```
$ sudo su
$ cd /tmp
$ git clone ssh://git@bitbucket.mrv.co.il:7999/misc/doc-book.git
$ rm -rf doc-book
$ exit
```

If `git clone` fails, ask BitBucket administrator to add `root`
user to BitBucket to enable `git clone`.


Fix libxml2
===========
If you worked before with OPX build on the current machine, then `libxml2.so.2.9.1`
(that was copied from somebody) is used via soft links `libxml2.so` and `libxml2.so.2`.

Otherwise, copy `libxml2.so.2.9.1` (32-bit) from somebody to `/usr/lib/i386-linux-gnu`.
Then create soft links as shown below:
```
$ cd /usr/lib/i386-linux-gnu
$ sudo rm libxml2.so libxml2.so.2
$ sudo ln -s libxml2.so.2.9.1 libxml2.so
$ sudo ln -s libxml2.so.2.9.1 libxml2.so.2
```

The end result has to be like below:
```
$ cd /usr/lib/i386-linux-gnu
$ find . -name "*libxml2*" | sort | xargs ls -ls
2544 -rw-r--r-- 1 root root 2602280 Sep 16 03:26 ./libxml2.a
   0 lrwxrwxrwx 1 root root      16 Dec  3 11:11 ./libxml2.so -> libxml2.so.2.9.1
   0 lrwxrwxrwx 1 root root      16 Dec  3 11:11 ./libxml2.so.2 -> libxml2.so.2.9.1
1380 -rw-r--r-- 1 root root 1409152 Dec  3 11:10 ./libxml2.so.2.9.1
1920 -rw-r--r-- 1 root root 1962112 Sep 16 03:26 ./libxml2.so.2.9.3
```


Fix `encoding.h`
================
```
$ sudo vim /usr/include/libxml2/libxml/encoding.h
```
Add `#undef LIBXML_ICU_ENABLED` on ~line 26.
So, that the code looks like below:

```
#ifndef __XML_CHAR_ENCODING_H__
#define __XML_CHAR_ENCODING_H__

#include <libxml/xmlversion.h>

#undef LIBXML_ICU_ENABLED

#ifdef LIBXML_ICONV_ENABLED
#include <iconv.h>
#endif
#ifdef LIBXML_ICU_ENABLED
```


Download and build Open Clovis
==============================
Now, download and install Open Clovis for Agema.
Here, we use the same github.com repository, but
different branch (`v6.0.1_mrv_tc_lk_4_4`).

```
$ git clone --branch=v6.0.1_mrv_tc_lk_4_4 ssh://git@bitbucket.mrv.co.il:7999/opx/safplus-availability-scalability-platform.git
$ cd safplus-availability-scalability-platform
$ ./install_oc_04_2015.sh # press 'enter' on every question
```

Note: The usual branch is `v6.0.1_mrv`.
But for Agema we use `v6.0.1_mrv_tc_lk_4_4`.


Git branches
============

| Project          | Git branch             |
|------------------|------------------------|
| main             | `agema`                |
| main_dev         | `feature/agema_master` |
| upgrade_director | `master`               |


Simple User-Space Only Build
============================
- Download all relevant git projects:
  ```
  $ git clone ssh://git@bitbucket.mrv.co.il:7999/opx/main.git
  $ git clone ssh://git@bitbucket.mrv.co.il:7999/opx/main_dev.git
  $ git clone ssh://git@bitbucket.mrv.co.il:7999/opx/upgrade_director.git
  ```
- Checkout appropriate git branches:
  - `main` project:
  ```
  $ git checkout -b agema origin/agema
  ```
  - `main_dev` project:
  ```
  $ git checkout -b feature/agema_master origin/feature/agema_master
  ```
  - `upgrade_director` project:
  ```
  $ git checkout -b master origin/master
  ```
- Build `main`:
  ```
  $ cd main
  $ sudo ./build_opx.py -c -f -w # -w is for whitebox
  ```
- We are done, take the bundle file from `tmp_local_build` directory
- To continue using `build_version.sh` script change ownership for all relevant files:
  ```
  $ cd work_dir_agema
  $ ls
  bcm_sdk_6.5.7 main main_dev upgrade_director tmp_local_build
  $ sudo chown -R <user_name>:<user_name>.
  ```


The End
=======

The End
=======

The End
=======

The End
=======

The End
=======

The below explains how to build the Agema project if Linux Kernel need to be changed
====================================================================================

The below explains how to build the Agema project if Linux Kernel need to be changed
====================================================================================

The below explains how to build the Agema project if Linux Kernel need to be changed
====================================================================================


Build OPX-Agema project
=======================
 - Build Linux Kernel
 - Build BCM SDK and install artifacts
 - Build 'main' project (produces bundle)


Build Linux Kernel
==================
```
$ git clone ssh://git@bitbucket.mrv.co.il:7999/opx/bcm_sdk_6.5.7.git
$ git clone ssh://git@bitbucket.mrv.co.il:7999/opx/main.git
$ git clone ssh://git@bitbucket.mrv.co.il:7999/opx/main_dev.git
$ git clone ssh://git@bitbucket.mrv.co.il:7999/opx/upgrade_director.git
```
Choose correct git branches in the relevant projects.
Verify the git branches with project leaders.

Clean stuff before build:
```
$ sudo rm -rf ~/root-fs-x86*
$ sudo rm -rf tmp_local_build
```
Note: `tmp_local_build` directory is created after local build.


Build BCM SDK and install artifacts
===================================

Note:

This phase is required only if BCM or linux kernel has been changed
and a rebuild of BCM is needed. Otherwise, skip it and move to
"Build 'main' project".

Anyway, if building BCM, make sure linux kernel is built:
```
$ cd main
$ sudo ./build_opx.py -c -f -w # -w is for whitebox
```

As the result of this build, Linux Kernel was built in your home directory
at `~/root-fs-x86/src/linux` and Linux Kernel headers were installed under
your `~/root-fs-x86/src/include` directory.

Clean `main` project from linux kernel objects:
```
$ cd main
$ find . -name "*.\ko" -exec rm {} \;
```

Clean BCM SDK project:
```
$ cd bcm_sdk
$ ./scripts/clean.sh . # mind the dot at the end!
```

Compile and copy BCM SDK artifacts to `main` project:
```
$ cd bcm_sdk
$ ./scripts/build.sh . ../main/3rd_party/bcm_artifacts/6.5.7
```


Build 'main' project (produces bundle)
======================================
Now all is simple:
```
$ cd main
$ sudo ./build_opx.py -c -f -w # -w is for whitebox
```

We are done - take the bundle file from `tmp_local_build` directory.


Build 'main' project via `build_version.sh` script
==================================================
Change ownership for all relevant files:
```
$ cd work_dir_agema
$ ls
bcm_sdk_6.5.7 main main_dev upgrade_director tmp_local_build
$ sudo chown -R <user_name>:<user_name> .
```

Note: Replace `<user_name>` with your user name.
Now, `build_version.sh` should succeed.
