Intro
=====
Agema build is based on Ubuntu 12.04 32-bit distribution.
So, when new system packages need to be added to Ageam they best be
taken from Ubuntu 12.04 32-bit. How it can be done described below.

Install virtualbox and Ubuntu 12.04 32-bit on it
================================================
Self-describing step.

Download `deb` package
======================
Ssh into Ubuntu 12.04 virtual machine and do the below:
```
sudo apt-get download <package>
```
For example, in case of `libfcgi-dev` the command is:
```
sudo apt-get download libfcgi0ldbl
```

Unpack `deb` archive
====================
The downloaded file is `libfcgi0ldbl_2.4.0-8.1ubuntu0.1_i386.deb`.
Now we need  to unpack it to any directory and that's all:
```
dpkg-deb -x libfcgi0ldbl_2.4.0-8.1ubuntu0.1_i386.deb /tmp/to_dir
```

Add to Agema build
==================
Actually, the easy way is to unpack `deb` archive straight to `main_dev`:
```
cd main_dev
dpkg-deb -x libfcgi0ldbl_2.4.0-8.1ubuntu0.1_i386.deb dist/target_x86/extras
```
