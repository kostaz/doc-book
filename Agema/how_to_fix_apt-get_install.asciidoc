= Here are steps to manually install NGINX on Agema

== Enable DNS resolution
Add in `/etc/resolvconf/resolv.conf.d/base`: `nameserver 8.8.8.8`.
Now, restart `resolvconf` service:
```
service resolvconf restart
```

== Fix the default OPX Ubuntu installation
```
apt-get -f install
dpkg -P libtext-charwidth-perl
dpkg -P libtext-iconv-perl
apt-get -f install
```

== Install nginx
Add in `/etc/apt/sources.list` at the end:
```
deb http://nginx.org/packages/mainline/ubuntu precise nginx
deb-src http://nginx.org/packages/mainline/ubuntu precise nginx
apt-get update
```
Perform on command line:
```
wget http://nginx.org/keys/nginx_signing.key
apt-key add nginx_signing.key
apt-get install nginx
```

== Fix nginx.conf
`vim /etc/nginx/nginx.conf` change paths to `/usr/deploy-cc/...`.

== Add iptables rule allowing port 80
```
iptables -A user -p tcp -m tcp --dport 80 -j ACCEPT
```

== Start nginx
```
service nginx start
```
