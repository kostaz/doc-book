Agema devices
==============
Power management access: `telnet 10.100.1.106` (credentials: `admin/admin`)

To reboot agema board: `reboot .A1`

Serial access: `telnet 10.100.1.105` (credentials: `InReach/access`)

| Device   | Power Port | Serial Port              | OOB address         |
|----------|------------|--------------------------|---------------------|
| A1       | A1         | telnet 10.100.1.105 2100 | telnet 10.100.1.242 |
| A2       | A2         | telnet 10.100.1.105 2200 | telnet 10.100.1.244 |
| A3       | A3         | telnet 10.100.1.105 2300 | telnet 10.100.1.241 |
| A4       | A4         | telnet 10.100.1.105 2400 | telnet 10.100.1.249 |
| A5       | A5         | telnet 10.100.1.105 2500 | telnet 10.100.1.245 |
| A6       | A6         | telnet 10.100.1.105 2600 | telnet 10.100.1.240 |


MRV-local virtual machines
==========================
Passwords for all machines: `123456`.

* Yocto with BCM SDK build:
  Disk: 87G
  `ssh kosta@172.21.10.140`

* Yocto QEMUx86
  Disk: 87G (no space left)
  `ssh bark@172.21.10.168`

* Yocto Raspberry Pi:
    - Disk: 180G
    - `ssh yocto@172.21.10.179`
    - Also for OS-V52

* Yocto Agema fresh build
  Disk: 180G
  `ssh yocto@172.21.10.180`

* Yocto Agema WebUI
  Disk: 180G
  `ssh yocto@172.21.10.114`

---

Add printer Xerox WorkCentre M123 (R&D 2nd floor)
=================================================
##### Define `root` password
```
sudo su
passwd
```

##### Login to CUPS
- Use Firefox browser! (Chrome demands Kerberos authentication.)
- `http://localhost:631/admin/login`
- Enter `root` and appropriate password

##### Add printer
- Administration
- Other Network Printers
- Internet Printing Protocol (http)
- Connection: `http://194.90.136.26/ipp` (Xerox WorkCentre M123)
- Name:        `Xerox_WorkCentre_M123`
- Description: `Xerox_WorkCentre_M123`
- Location:    `Xerox_WorkCentre_M123`
- Make: `Xerox` (and press `Continue`)
- Model: `Xerox WorkCentre M118 - CUPS+Gutenprint v5.2.13 (en)`
- Press `Add Printer`
