EDE questions for Adva
======================

- Agema product is built for i386 target (32-bit), but `ede_modules_config`
  does not support something like `arch--i386-linux-gnu` (not seen in GUI menu).
  Is it possible to add `arch--i386-linux-gnu` (32-bit) target to EDE?
  How much time it takes to add this x86 32-bit (i386) architecture?
  Note: The root cause here is that BCM SDK (Broadcom SDK) does not support
  x86_64 target. So, we have to use x86 32-bit.

- How to compile our own Agema Linux Kernel 4.12 in EDE environment?
  We have Linux Kernel 4.12 with a bunch of patches to support Agema board.
  Out Linux Kernel was tested and works ok on Agema board. So, we would like
  to use it at least for a short term.

- If we want to change Linux Kernel configuration (`.config` file), do we do it
  via EDE or AOS?


```
[   74.308756] linux_kernel_bde: loading out-of-tree module taints kernel.
[   74.316336] linux_kernel_bde: module license 'Proprietary' taints kernel.
[   74.324068] Disabling lock debugging due to kernel taint
[   74.331365] linux-kernel-bde (3083): _alloc_mpool: size               = [0x01000000]
[   74.340172] linux-kernel-bde (3083): _alloc_mpool: alloc_size         = [0x00800000]
[   74.348854] linux-kernel-bde (3083): _alloc_mpool: _dma_mem_size      = [0x01000000]
[   74.357585] linux-kernel-bde (3083): _alloc_mpool: MAX_ORDER          = [0x0000000c]
[   74.366349] linux-kernel-bde (3083): _alloc_mpool: PAGE_SHIFT         = [0x0000000c]
[   74.375090] linux-kernel-bde (3083): _alloc_mpool: DMA_MAX_ALLOC_SIZE = [0x00800000]
[   74.386725] linux-kernel-bde (3083): _alloc_mpool: allocated 0x800000 bytes instead of 0x1000000 bytes.
```


CONFIG_FORCE_MAX_ZONEORDER
==========================

[https://github.com/pedwo/buildroot/wiki/Memory-Allocation-for-Hardware]

The maximum amount of memory that can be allocated to a single device is
controlled by the kernel option CONFIG_FORCE_MAX_ZONEORDER.

 * if CONFIG_FORCE_MAX_ZONEORDER == 12 then max alloc is 8MB
 * if CONFIG_FORCE_MAX_ZONEORDER == 13 then max alloc is 16MB


Open Agema bundle manually
==========================
```
export bundle=5.5.1.7901.bundle
export BOX_TYPE=WhiteBox
ARCHIVE=`awk '/^__ARCHIVE_BELOW__/ {print NR + 1; exit 0; }' $bundle`
export ARCHIVE
export bundle_dir="${bundle}_dir"
mkdir $bundle_dir
tail -n+$ARCHIVE $bundle | tar -xpzm -C "$bundle_dir"
```


ONL modules
===========
Porting Linux Kernel modules from Open Network Linux (ONL) to Agema/OPX build.

In Linux Kernel 4.12 the I2C subsystem uses `i2c_mux_add_adapter()` function:

Linux Kernel 4.12
-----------------
```
drivers/i2c/i2c-mux.c:282
int i2c_mux_add_adapter(struct i2c_mux_core *muxc,
                        u32 force_nr, u32 chan_id,
                        unsigned int class)
```

But in ONL Linux Kernel 3.16 the modules use `i2c_add_mux_adapter()` function:

Linux Kernel 3.16
-----------------
```
drivers/i2c/i2c-mux.c:103
struct i2c_adapter *i2c_add_mux_adapter(struct i2c_adapter *parent,
                                struct device *mux_dev,
                                void *mux_priv, u32 force_nr, u32 chan_id,
                                unsigned int class,
                                int (*select) (struct i2c_adapter *,
                                               void *, u32),
                                int (*deselect) (struct i2c_adapter *,
                                                 void *, u32))
```

The example of driver code conversion from `i2c_add_mux_adapter()` to
`i2c_mux_add_adapter()` is below. The conversion itself is called
I2C client binding.

I2C client binding
------------------
```
git show --stat f01919e8f54f645fb00fdb823fe266e21eebe3b1 | tee
commit f01919e8f54f645fb00fdb823fe266e21eebe3b1
Author: Antti Palosaari <crope@iki.fi>
Date:   Thu Apr 16 20:04:55 2015 -0300

    [media] m88ds3103: add I2C client binding

    Implement I2C client device binding.
    Wrap media attach to driver I2C probe.
    Add wrapper from m88ds3103_attach() to m88ds3103_probe() via driver
    core in order to provide proper I2C client for legacy media attach
    binding.

    Signed-off-by: Antti Palosaari <crope@iki.fi>
    Signed-off-by: Mauro Carvalho Chehab <mchehab@osg.samsung.com>

 drivers/media/dvb-frontends/m88ds3103.c      | 268 +++++++++++++++++++--------
 drivers/media/dvb-frontends/m88ds3103.h      |  63 ++++++-
 drivers/media/dvb-frontends/m88ds3103_priv.h |   2 +
 3 files changed, 245 insertions(+), 88 deletions(-)
```


Yocto vs EDE
============

Yocto Pros
----------
- x86 32-bit arch is supported out-of-box
- Linux Kernel 4.12 is supported out-of-box
- RootFS creation is easy (`bitbake core-image-minimal`)
- Gcc version is 7.2.0 (Aug, 2017) or 6.3.0 (Dec, 2016)
- Toolchain create is easy (`bitbake meta-toolchain`)

EDE
---



Yocto Toolchain
===============
### Create toolchain
```
source ./oe-init-build-env ../build
bitbake meta-toolchain
```

### Install toolchain
```
cd tmp/deploy/sdk
sudo cp poky-glibc-x86_64-meta-toolchain-core2-32-toolchain-2.4.sh /opt
cd /opt
./poky-glibc-x86_64-meta-toolchain-core2-32-toolchain-2.4.sh
```

### Use toolchain
```
cd /opt/poky/2.4
source ./environment-setup-core2-32-poky-linux
```

#### Toolchain environment
When using Yocto toolchain (`source ./environment-setup-core2-32-poky-linux`),
new environment variable are defined:
```
AR=i686-poky-linux-ar
ARCH=x86
AS='i686-poky-linux-as  '
CC='i686-poky-linux-gcc  -m32 -march=core2 -mtune=core2 -msse3 -mfpmath=sse --sysroot=/opt/poky/2.4/sysroots/core2-32-poky-linux'
CFLAGS=' -O2 -pipe -g -feliminate-unused-debug-types '
CONFIGURE_FLAGS='--target=i686-poky-linux --host=i686-poky-linux --build=x86_64-linux --with-libtool-sysroot=/opt/poky/2.4/sysroots/core2-32-poky-linux'
CONFIG_SITE=/opt/poky/2.4/site-config-core2-32-poky-linux
CPP='i686-poky-linux-gcc -E  -m32 -march=core2 -mtune=core2 -msse3 -mfpmath=sse --sysroot=/opt/poky/2.4/sysroots/core2-32-poky-linux'
CPPFLAGS=''
CROSS_COMPILE=i686-poky-linux-
CXX='i686-poky-linux-g++  -m32 -march=core2 -mtune=core2 -msse3 -mfpmath=sse --sysroot=/opt/poky/2.4/sysroots/core2-32-poky-linux'
CXXFLAGS=' -O2 -pipe -g -feliminate-unused-debug-types '
GDB=i686-poky-linux-gdb
KCFLAGS='--sysroot=/opt/poky/2.4/sysroots/core2-32-poky-linux'
LD='i686-poky-linux-ld   --sysroot=/opt/poky/2.4/sysroots/core2-32-poky-linux'
LDFLAGS='-Wl,-O1 -Wl,--hash-style=gnu -Wl,--as-needed'
M4=m4
NM=i686-poky-linux-nm
OBJCOPY=i686-poky-linux-objcopy
OBJDUMP=i686-poky-linux-objdump
OECORE_ACLOCAL_OPTS='-I /opt/poky/2.4/sysroots/x86_64-pokysdk-linux/usr/share/aclocal'
OECORE_DISTRO_VERSION=2.4
OECORE_NATIVE_SYSROOT=/opt/poky/2.4/sysroots/x86_64-pokysdk-linux
OECORE_SDK_VERSION=2.4
OECORE_TARGET_SYSROOT=/opt/poky/2.4/sysroots/core2-32-poky-linux
PATH=/opt/poky/2.4/sysroots/x86_64-pokysdk-linux/usr/bin:/opt/poky/2.4/sysroots/x86_64-pokysdk-linux/usr/sbin:/opt/poky/2.4/sysroots/x86_64-pokysdk-linux/bin:/opt/poky/2.4/sysroots/x86_64-pokysdk-linux/sbin:/opt/poky/2.4/sysroots/x86_64-pokysdk-linux/usr/bin/../x86_64-pokysdk-linux/bin:/opt/poky/2.4/sysroots/x86_64-pokysdk-linux/usr/bin/i686-poky-linux:/opt/poky/2.4/sysroots/x86_64-pokysdk-linux/usr/bin/i686-poky-linux-musl:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/home/mrv/go/bin:/home/mrv/.fzf/bin
PKG_CONFIG_PATH=/opt/poky/2.4/sysroots/core2-32-poky-linux/usr/lib/pkgconfig:/opt/poky/2.4/sysroots/core2-32-poky-linux/usr/share/pkgconfig
PKG_CONFIG_SYSROOT_DIR=/opt/poky/2.4/sysroots/core2-32-poky-linux
RANLIB=i686-poky-linux-ranlib
SDKTARGETSYSROOT=/opt/poky/2.4/sysroots/core2-32-poky-linux
STRIP=i686-poky-linux-strip
TARGET_PREFIX=i686-poky-linux-
```
