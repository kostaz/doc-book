Please see below the explanation on how to test USB DoK on NextGen boards.


Preparation
===========
Format USB DoK on Linux PC in EXT3 file system.


Test on NG board
================
After NG board boots - enter into Linux shell and
clean dmesg by command:
```
dmesg -c
```

Enable Linux log to maximum verbose mode:
```
$ dmesg -n8
```

Insert USB DoK in NG target - you should see the log like below:
```
usb 1-1: new high-speed USB device number 2 using ehci_hcd
scsi0 : usb-storage 1-1:1.0
scsi 0:0:0:0: Direct-Access     SanDisk  Cruzer Blade     1.26 PQ: 0 ANSI: 5
sd 0:0:0:0: [sda] 7821312 512-byte logical blocks: (4.00 GB/3.72 GiB)
sd 0:0:0:0: [sda] Write Protect is off
sd 0:0:0:0: [sda] Mode Sense: 43 00 00 00
sd 0:0:0:0: [sda] Write cache: disabled, read cache: enabled, doesn't support DPO or FUA
 sda: sda1
sd 0:0:0:0: [sda] Attached SCSI removable disk
```

Mount USB DoK:
```
$ mkdir /tmp/sda1
$ mount /dev/sda1 /tmp/sda1
$ cd /tmp/sda1
```

Create, copy, delete, etc. any files in `/tmp/sda1`.
Verify that all changes are fully performed by:
```
$ sync
```

Umount USB DoK by:
```
$ cd ~
$ umount /dev/sda1
```

Insert USB DoK into Linux PC and verify that all the changes are there.
