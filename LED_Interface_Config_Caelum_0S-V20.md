LED configuration for Caelum and BobCat2
========================================

- The copy-paste script below configures mappings of ports 36-39 to LED stream.
- Ports 36-39 LED stream mapping takes the place of ports 68-71 in LED stream.
    - See `Port MAC Control Register4` bits[15:10] called `LEDs Number`.
- Ports 68-71 LED stream mapping is configured to "not mapped".
    - See `XG MIB Counters Control Register` bits[10:5] called `leds_port_num`.
- Port 37, 38 and 39 copy MAC-based configuration from port 36.
    - Because port 36 was correctly configured by CPSS software previosly.

```
debug
hw-debug

# Port Auto-Negotiation Configuration Register
set memory 0 1002500C 0000926C # Port 37
set memory 0 1002600C 0000926C # Port 38
set memory 0 1002700C 0000926C # Port 39

# Port MAC Control Register0
set memory 0 10025000 0000D003 # Port 37
set memory 0 10026000 0000D003 # Port 38
set memory 0 10027000 0000D003 # Port 39

# Port MAC Control Register1
set memory 0 10025004 00007941 # Port 37
set memory 0 10026004 00007941 # Port 38
set memory 0 10027004 00007941 # Port 39

# Port MAC Control Register2
set memory 0 10025008 0000C008 # Port 37
set memory 0 10026008 0000C008 # Port 38
set memory 0 10027008 0000C008 # Port 39

# Exchange Port 36 LED stream with 68 (36 <-> 68)
set memory 0 102CC030 000007EC # Port 68 (non-present in LED stream)
set memory 0 10024090 00002102 # Port 36 new place in LED stream 8 (= 0x8) bits[15:10]
set memory 0 10024004 7941     # Port 36 Port MAC Control Register1 PCS LoopBackEn
set memory 0 50000120 55545555 # Set port type to GIG for LED streams 11-8

# Exchange Port 37 LED stream with 69 (37 <-> 69)
set memory 0 102CD030 000007EC # Port 69 (non-present in LED stream)
set memory 0 10025090 00002502 # Port 37 new place in LED stream 9 (= 0x9) bits[15:10]
set memory 0 10025004 7941     # Port 37 Port MAC Control Register1 PCS LoopBackEn
set memory 0 50000120 55505555 # Set port type to GIG for LED streams 11-8

# Exchange Port 38 LED stream with 70 (38 <-> 70)
set memory 0 102CE030 000007EC # Port 70 (non-present in LED stream)
set memory 0 10026090 00002902 # Port 38 new place in LED stream 10 (= 0xA) bits[15:10]
set memory 0 10026004 7941     # Port 38 Port MAC Control Register1 PCS LoopBackEn
set memory 0 50000120 55405555 # Set port type to GIG for LED streams 11-8

# Exchange Port 39 LED stream with 71 (39 <-> 71)
set memory 0 102CF030 000007EC # Port 71 (non-present in LED stream)
set memory 0 10027090 00002D02 # Port 39 new place in LED stream 11 (= 0xB) bits[15:10]
set memory 0 10027004 7941     # Port 39 Port MAC Control Register1 PCS LoopBackEn
set memory 0 50000120 55005555 # Set port type to GIG for LED streams 11-8
```

---

#### Retrieve placement of Port 69 and 70 in LED stream

#### Port 69
- 69 - 56 = 13 decimal => 0xD
- Register address: 100C0030 + 200000 + 1000 * D = 102CD030
- Original value bit[10:5] = **001001b** (= 0x9)
- Conclusion: Placement of Port 69 in LED stream is 001001b (8 decimal)

```
 OS-V20-F(hw-debug)# show memory 0 102CD030
  Device: 0
 Address: 0x102CD030 =  0x0000012C
31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 0 1 0 0 1 0 1 1 0 0
```

#### Port 70
- 70 - 56 = 14 decimal => 0xE
- Register address: 100C0030 + 200000 + 1000 * E = 102CE030
- Original value bit[10:5] = **001010b** (= 0xA)
- Conclusion: Placement of Port 70 in LED stream is 001010b (10 decimal)

```
OS-V20-F(hw-debug)# show memory 0 102CE030
  Device: 0
 Address: 0x102CE030 =  0x0000014C
31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 0 1 0 1 0 0 1 1 0 0
```

---

#### Exchange placement of Port 69 and 70 in LED stream

To exchange the place of Ports 69 and 70 in LED stream:
- Port 69: Set bit[10:5] = 001010b
- Port 70: Set bit[10:5] = 001001b

```
set memory 0 102CD030 0000014C # Port 69 new place in LED stream
set memory 0 102CE030 0000012C # Port 70 new place in LED stream
```

To restore original place of Ports 69 and 70 in LED stream:
- Port 69: Set bit[10:5] = 001001b
- Port 70: Set bit[10:5] = 001010b

```
set memory 0 102CD030 0000012C # Port 69 orig place in LED stream
set memory 0 102CE030 0000014C # Port 70 orig place in LED stream
```

---

#### Retrieve placement of Port 0 and 1 in LED stream

#### Port 0
- Port MAC Control Register4 (g=0–47, g=56–59, g=62–62, g=64–71)
- Formula: 0x10000090 + 0x1000 * g: where g (0-47) represents Port
- Register address: 10000090 + 1000 * 0 = 10000090
- Original value of LEDs Number (bit[15:10]) = **000000b**
- Conclusion: Placement of Port 0 in LED stream is 000000b (0 decimal)

```
 OS-V20-F(hw-debug)# show memory 0 10000090
  Device: 0
 Address: 0x10000090 =  0x00000102
31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 0 1 0 0 0 0 0 0 1 0
```

#### Port 1
- Port MAC Control Register4 (g=0–47, g=56–59, g=62–62, g=64–71)
- Formula: 0x10000090 + 0x1000 * g: where g (0-47) represents Port
- Register address: 10000090 + 1000 * 1 = 10001090
- Original value of LEDs Number (bit[15:10]) = **000001b**
- Conclusion: Placement of Port 1 in LED stream is 000001b (1 decimal)

```
 OS-V20-F(hw-debug)# show memory 0 10001090
  Device: 0
 Address: 0x10001090 =  0x00000502
31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1 0 1 0 0 0 0 0 0 1 0
```

---

#### Exchange placement of Port 0 and 1 in LED stream

To exchange the place of Ports 0 and 1 in LED stream:
- Port 0: Set bit[15:10] = 000001b
- Port 1: Set bit[15:10] = 000000b

```
set memory 0 10000090 00000502 # Port 0 new place in LED stream
set memory 0 10001090 00000102 # Port 1 new place in LED stream
```

To restore original place of Ports 0 and 1 in LED stream:
- Port 0: Set bit[15:10] = 000000b
- Port 1: Set bit[15:10] = 000001b

```
set memory 0 10000090 00000102 # Port 0 orig place in LED stream
set memory 0 10001090 00000502 # Port 1 orig place in LED stream
```

---

#### Retrieve placement of Port 36 and 70 in LED stream

#### Port 36 (not used in Caelum OS-V20)
- Port MAC Control Register4 (g=0–47, g=56–59, g=62–62, g=64–71)
- Formula: 0x10000090 + 0x1000 * g: where g (0-47) represents Port
- 36 decimal = 0x24 (hex)
- Register address: 10000090 + 1000 * 24 = 10024090
- Original value of `LEDs Number` (bit[15:10]) = **111111b**
- Conclusion: Placement of Port 36 in LED stream is 111111b (63 decimal) (non-present in LED stream)

```
 OS-V20-F(hw-debug)# show memory 0 10024090
  Device: 0
 Address: 0x10024090 =  0x0000FD02
31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1  1  1  1  1  1 0 1 0 0 0 0 0 0 1 0
```

#### Port 70
- 70 - 56 = 14 decimal => 0xE
- Register address: 100C0030 + 200000 + 1000 * E = 102CE030
- Original value bit[10:5] = **001010b**
- Conclusion: Placement of Port 70 in LED stream is 001010b (10 decimal)

```
OS-V20-F(hw-debug)# show memory 0 102CE030
  Device: 0
 Address: 0x102CE030 =  0x0000014C
31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 0 1 0 1 0 0 1 1 0 0
```

---

Map only one port 36 instead of port 71 in LED stream
=====================================================

#### Exchange placement of Port 36 and 70 in LED stream

To exchange the place of Ports 36 and 70 in LED stream:
- Port 36: Set `LEDs Number` bit[15:10] = 001010b (0xA in hex)
- Port 70: Set `leds_port_num` bit[10:5] = 111111b (non-present in LED stream)

```
set memory 0 102CE030 000007EC # Port 70 new place in LED stream (non-present in LED stream)
set memory 0 10024090 00002902 # Port 36 new place in LED stream
```

To restore original place of Ports 36 and 70 in LED stream:
- Port 36: Set `LEDs Number` bit[15:10] = 111111b (non-present in LED stream)
- Port 70: Set `leds_port_num` bit[10:5] = 001010b

```
set memory 0 10024090 0000FD02 # Port 36 orig place in LED stream (non-present in LED stream)
set memory 0 102CE030 0000014C # Port 70 orig place in LED stream
```

---

Map four ports 36-39 instead of ports 68-71 in LED stream
=========================================================

#### Exchange Port 36 LED stream with 68 (36 <-> 68)

```
set memory 0 102CC030 000007EC # Port 68 (non-present in LED stream)
set memory 0 10024090 00002102 # Port 36 new place in LED stream 8 (= 0x8) bits[15:10]
set memory 0 10024004 7941     # Port 36 Port MAC Control Register1 PCS LoopBackEn
set memory 0 50000120 55545555 # Set port type to GIG for LED streams 11-8
```

#### Exchange Port 37 LED stream with 69 (37 <-> 69)

```
set memory 0 102CD030 000007EC # Port 69 (non-present in LED stream)
set memory 0 10025090 00002502 # Port 37 new place in LED stream 9 (= 0x9) bits[15:10]
set memory 0 10025004 7941     # Port 37 Port MAC Control Register1 PCS LoopBackEn
set memory 0 50000120 55505555 # Set port type to GIG for LED streams 11-8
```

#### Exchange Port 38 LED stream with 70 (38 <-> 70)

```
set memory 0 102CE030 000007EC # Port 70 (non-present in LED stream)
set memory 0 10026090 00002902 # Port 38 new place in LED stream 10 (= 0xA) bits[15:10]
set memory 0 10026004 7941     # Port 38 Port MAC Control Register1 PCS LoopBackEn
set memory 0 50000120 55405555 # Set port type to GIG for LED streams 11-8
```

#### Exchange Port 39 LED stream with 71 (39 <-> 71)

```
set memory 0 102CF030 000007EC # Port 71 (non-present in LED stream)
set memory 0 10027090 00002D02 # Port 39 new place in LED stream 11 (= 0xB) bits[15:10]
set memory 0 10027004 7941     # Port 39 Port MAC Control Register1 PCS LoopBackEn
set memory 0 50000120 55005555 # Set port type to GIG for LED streams 11-8
```

---

Check Port Registers
====================

#### Port Status Register0

```
show memory 0 10024010 # Port 36 (10000010 + 1000 * 24 = 10024010)
show memory 0 10025010 # Port 37 (10000010 + 1000 * 25 = 10025010)
show memory 0 10026010 # Port 38 (10000010 + 1000 * 26 = 10026010)
show memory 0 10027010 # Port 39 (10000010 + 1000 * 27 = 10027010)

0x10024010 = 0x0000680F
31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1  1  0  1  0 0 0 0 0 0 0 1 1 1 1

0x10025010 = 0x00002C3A
0x10026010 = 0x00002C3A
0x10027010 = 0x00002C3A
31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1  0  1  1 0 0 0 0 1 1 1 0 1 0
```

#### Port Status Register1

```
show memory 0 10024040 # Port 36 (10000040 + 1000 * 24 = 10024040)
show memory 0 10025040 # Port 37 (10000040 + 1000 * 25 = 10025040)
show memory 0 10026040 # Port 38 (10000040 + 1000 * 26 = 10026040)
show memory 0 10027040 # Port 39 (10000040 + 1000 * 27 = 10027040)

0x10024040 =  0x00008000
0x10025040 =  0x00008000
0x10026040 =  0x00008000
0x10027040 =  0x00008000
31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1  0  0  0  0  0 0 0 0 0 0 0 0 0 0 0
```

---

Configure Ports 37, 38, 39 as Port 36
=====================================

Note: For shortcut - follow `Commands for copy-paste`.

#### GbE MAC Registers configuration
##### Port Auto-Negotiation Configuration Register
- Formula: 0x1000000C + 0x1000 * g: where g (0-47) represents Port
- 36 decimal = 0x24 (hex)
- Port 36: Register address: 1000000C + 1000 * 24 = 1002400C
- Port 37: Register address: 1000000C + 1000 * 25 = 1002500C
- Port 38: Register address: 1000000C + 1000 * 26 = 1002600C
- Port 39: Register address: 1000000C + 1000 * 27 = 1002700C

```
show memory 0 1002400C # Port 36
show memory 0 1002500C # Port 37
show memory 0 1002600C # Port 38
show memory 0 1002700C # Port 39
```

```
0x1002400C = 0x0000926C
31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1  0  0  1  0  0 1 0 0 1 1 0 1 1 0 0

0x1002500C = 0x0000B8E8
0x1002600C = 0x0000B8E8
0x1002700C = 0x0000B8E8
31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1  0  1  1  1  0 0 0 1 1 1 0 1 0 0 0
```

##### Commands for copy-paste

```
set memory 0 1002500C 0000926C # Port 37
set memory 0 1002600C 0000926C # Port 38
set memory 0 1002700C 0000926C # Port 39
```

##### Port Serial Parameters Configuration Register
- Formula: 0x10000014 + 0x1000 * g: where g (0-47) represents Port
- 36 decimal = 0x24 (hex)
- Port 36: Register address: 10000014 + 1000 * 24 = 10024014
- Port 37: Register address: 10000014 + 1000 * 25 = 10025014
- Port 38: Register address: 10000014 + 1000 * 26 = 10026014
- Port 39: Register address: 10000014 + 1000 * 27 = 10027014

```
show memory 0 10024014 # Port 36
show memory 0 10025014 # Port 37
show memory 0 10026014 # Port 38
show memory 0 10027014 # Port 39
```

##### Port Serial Parameters 1 Configuration Register
- Formula: 0x10000094 + 0x1000 * g: where g (0-47) represents Port
- 36 decimal = 0x24 (hex)
- Port 36: Register address: 10000094 + 1000 * 24 = 10024094
- Port 37: Register address: 10000094 + 1000 * 25 = 10025094
- Port 38: Register address: 10000094 + 1000 * 26 = 10026094
- Port 39: Register address: 10000094 + 1000 * 27 = 10027094

```
show memory 0 10024094 # Port 36
show memory 0 10025094 # Port 37
show memory 0 10026094 # Port 38
show memory 0 10027094 # Port 39
```

##### Port SERDES Configuration Register0
- Formula: 0x10000028 + 0x1000 * g: where g (0-47) represents Port
- 36 decimal = 0x24 (hex)
- Port 36: Register address: 10000028 + 1000 * 24 = 10024028
- Port 37: Register address: 10000028 + 1000 * 25 = 10025028
- Port 38: Register address: 10000028 + 1000 * 26 = 10026028
- Port 39: Register address: 10000028 + 1000 * 27 = 10027028

```
show memory 0 10024028 # Port 36
show memory 0 10025028 # Port 37
show memory 0 10026028 # Port 38
show memory 0 10027028 # Port 39
```

##### Pulse 1 ms Low Register
- Formula: 0x100000D4 + 0x1000 * g: where g (0-47) represents Port
- 36 decimal = 0x24 (hex)
- Port 36: Register address: 100000D4 + 1000 * 24 = 100240D4
- Port 37: Register address: 100000D4 + 1000 * 25 = 100250D4
- Port 38: Register address: 100000D4 + 1000 * 26 = 100260D4
- Port 39: Register address: 100000D4 + 1000 * 27 = 100270D4

```
show memory 0 100240D4 # Port 36
show memory 0 100250D4 # Port 37
show memory 0 100260D4 # Port 38
show memory 0 100270D4 # Port 39
```

##### Pulse 1 ms High Register
- Formula: 0x100000D8 + 0x1000 * g: where g (0-47) represents Port
- 36 decimal = 0x24 (hex)
- Port 36: Register address: 100000D8 + 1000 * 24 = 100240D8
- Port 37: Register address: 100000D8 + 1000 * 25 = 100250D8
- Port 38: Register address: 100000D8 + 1000 * 26 = 100260D8
- Port 39: Register address: 100000D8 + 1000 * 27 = 100270D8

```
show memory 0 100240D8 # Port 36
show memory 0 100250D8 # Port 37
show memory 0 100260D8 # Port 38
show memory 0 100270D8 # Port 39
```

##### Port MAC Control Register0
- Formula: 0x10000000 + 0x1000 * g: where g (0-47) represents Port
- 36 decimal = 0x24 (hex)
- Port 36: Register address: 10000000 + 1000 * 24 = 10024000
- Port 37: Register address: 10000000 + 1000 * 25 = 10025000
- Port 38: Register address: 10000000 + 1000 * 26 = 10026000
- Port 39: Register address: 10000000 + 1000 * 27 = 10027000

```
show memory 0 10024000 # Port 36
show memory 0 10025000 # Port 37
show memory 0 10026000 # Port 38
show memory 0 10027000 # Port 39
```

```
0x10024000 = 0x0000D003
31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1  1  0  1  0  0 0 0 0 0 0 0 0 0 1 1

0x10025000 = 0x00008BE5
0x10026000 = 0x00008BE5
0x10027000 = 0x00008BE5
31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1  0  0  0  1  0 1 1 1 1 1 0 0 1 0 1
```

##### Commands for copy-paste

```
set memory 0 10025000 0000D003 # Port 37
set memory 0 10026000 0000D003 # Port 38
set memory 0 10027000 0000D003 # Port 39
```

##### Port MAC Control Register1
- Formula: 0x10000004 + 0x1000 * g: where g (0-47) represents Port
- 36 decimal = 0x24 (hex)
- Port 36: Register address: 10000004 + 1000 * 24 = 10024004
- Port 37: Register address: 10000004 + 1000 * 25 = 10025004
- Port 38: Register address: 10000004 + 1000 * 26 = 10026004
- Port 39: Register address: 10000004 + 1000 * 27 = 10027004

```
show memory 0 10024004 # Port 36
show memory 0 10025004 # Port 37
show memory 0 10026004 # Port 38
show memory 0 10027004 # Port 39
```

##### Commands for copy-paste

```
set memory 0 10025004 00007941 # Port 37
set memory 0 10026004 00007941 # Port 38
set memory 0 10027004 00007941 # Port 39
```

##### Port MAC Control Register2
- Formula: 0x10000008 + 0x1000 * g: where g (0-47) represents Port
- 36 decimal = 0x24 (hex)
- Port 36: Register address: 10000008 + 1000 * 24 = 10024008
- Port 37: Register address: 10000008 + 1000 * 25 = 10025008
- Port 38: Register address: 10000008 + 1000 * 26 = 10026008
- Port 39: Register address: 10000008 + 1000 * 27 = 10027008

```
show memory 0 10024008 # Port 36
show memory 0 10025008 # Port 37
show memory 0 10026008 # Port 38
show memory 0 10027008 # Port 39
```

##### Commands for copy-paste

```
set memory 0 10025008 0000C008 # Port 37
set memory 0 10026008 0000C008 # Port 38
set memory 0 10027008 0000C008 # Port 39
```
