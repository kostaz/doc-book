STEP 1: create your user branch (feature or bugfix)
===================================================

- Create branch `my_feature` on BitBucket
- `$ git clone ...`
- `$ git checkout my_feature`


STEP 2: developing your feature
===============================
- `$ git add ...`
- `$ git commit ...`

Note:
During your development, usually `master` branch is updated on the server
by other developers. So you need to merger latest master changes
into `my_feature` branch.


STEP 3: UPDATE your local master
================================
- `$ git checkout master`
- `$git fetch`
- `$git merge FETCH_HEAD`
- `$git checkout my_feature`
- `$git merge master`


STEP 4: PUSH: push you local branch to "server"
===============================================
- `$git push origin my_feature`


STEP 5: MERGE: merge your updated branch on the server into the master branch
=============================================================================
Open a Pull Request on BitBucket from your branch to `master` branch.
Add reviewers to your Pull Request.


STEP 6: Handle Pull Request's comments from your reviewers
==========================================================
Fix your branch by repeating steps 2-4.


STEP 7: Merge your branch into Master on BitBucket
==================================================
When all fixes are done and the Pull Request is approved, "merge" it on BitBucket.
If feature/bug_fix is done, please delete it from BitBucket after the merge.
